<?php
require_once("include/config/config.php");
include DIR_FS_SITE . 'include/class/adminlog.php';
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/buyerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/otherSettingClass.php');
if (!$admin_user->is_logged_in()) {
    Redirect(DIR_WS_SITE . 'index.php');
}
if ($_SESSION['admin_session_secure']['user_type'] == 'user') {
    Redirect(DIR_WS_SITE . 'user.php');
}
require 'tmp/form_actions.php';
require 'tmp/header.php';
?>
<body id="mobile_wrap">
    <div class="statusbar-overlay"></div>
    <div class="panel-overlay"></div>
    <!-- Views -->
    <div class="views">
        <div class="view view-main">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="left nav_left_logo">
                        <a class="link" href="<?php echo make_load_url('home') ?>"><img src="assets/images/logo.png" alt="" title=""></a>
                    </div>
                    <div class="right" style="color: #fff;margin-right: 10px;">
                        <font style="font-size: 15px">Logged In as</font><br/>
                        <center>
                            <b><?php echo ucfirst($_SESSION['admin_session_secure']['username']); ?></b>
                        </center>
                    </div>
                </div>
            </div>
            <div class="pages navbar-through toolbar-through">
                <div data-page="home" class="page" style="overflow: auto;background: #1D3D76;">
                    <div class="page-content" style="overflow: auto;padding-top: 20px;padding-right: 10px;padding-left: 10px;">
                        <div class="list-block">
                            <div class="row" style="margin-top: 60px;">
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('buyer') ?>"><img src="assets/images/icons/white/buyer.png" alt="" title="" />
                                            <br/>
                                            <span>Buyers</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('sale_head') ?>"><img src="assets/images/icons/white/sale_head.png" alt="" title="" />
                                            <br/>
                                            <span>Sale Name</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('horse') ?>"><img src="assets/images/icons/white/lots.png" alt="" title="" />
                                            <br/>
                                            <span>Lots</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('sale') ?>"><img src="assets/images/icons/white/sales.png" alt="" title="" />
                                            <br/>
                                            <span>Results</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('user') ?>"><img src="assets/images/icons/white/team.png" alt="" title="" />
                                            <br/>
                                            <span>Users</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('report') ?>"><img src="assets/images/icons/white/report.png" alt="" title="" />
                                            <br/>
                                            <span>Reports</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('setting') ?>"><img src="assets/images/icons/white/settings.png" alt="" title="" />
                                            <br/>
                                            <span>Setting</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('view') ?>"><img src="assets/images/icons/white/sales.png" alt="" title="" />
                                            <br/>
                                            <span>Views</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('about') ?>"><img src="assets/images/icons/white/about.png" alt="" title="" />
                                            <br/>
                                            <span>About</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-33">
                                    <div class="menu_content">
                                        <a style="color: #fff;" class="item-link" onclick="location.href = '<?php echo make_admin_url('logout'); ?>'" style="cursor: pointer"><img src="assets/images/icons/white/lock.png" alt="" title="" />
                                            <br/>
                                            <span>Logout</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require 'tmp/footer.php'; ?>
    <script>
        $(document).on('click', '#add_more_select', function () {
            var html = '';
            html += '<div id="scrolltop"><select name="buyer_id[]" class="chosen buyer_data_id" style="width: 80%">';
<?php
$obj = new buyer;
$buyers = $obj->listBuyers();
foreach ($buyers as $buyer) {
    ?>
                html += '<option value="<?php echo $buyer->id; ?>" data-name="<?php echo str_replace("'", "", ucfirst($buyer->first_name)); ?> <?php echo str_replace("'", "", ucfirst($buyer->last_name)); ?>"><?php echo str_replace("'", "", ucfirst($buyer->first_name)); ?> <?php echo str_replace("'", "", ucfirst($buyer->last_name)); ?></option>';
<?php } ?>
            html += '</select><span style="margin-left: 10px;color: #3b5998" id="remove_select_buyers"><i style="font-size: 50px;" class="fa fa-times-circle"></i></span></div>';
            $('#append_fields').append(html);
            $(".chosen").data("placeholder", "Select Buyer...").chosen();
            $('#append_fields').find('div#scrolltop:last').hide().show(300);
            $('.custom_button:not(:last)').hide();
            $('.chzn-container.chzn-container-single:last').css('width', '80%');
        });

    </script>
</body>
<!-- END BODY -->
</html>