var myApp = new Framework7({
    animateNavBackIcon: true,
    precompileTemplates: true,
    swipeBackPage: false,
    swipeBackPageThreshold: 1,
    swipePanel: "left",
    modalTitle: "KHBSales",
    swipePanelCloseOpposite: true,
    pushState: true,
    pushStateRoot: undefined,
    pushStateNoAnimation: false,
    pushStateSeparator: '#!/',
    template7Pages: true,
    smartSelectInPopup: true,
    onAjaxStart: function (xhr) {
        myApp.showIndicator();
    },
    onAjaxComplete: function (xhr) {
        myApp.hideIndicator();
    }
});
var $$ = Dom7;
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true,
    force: true,
    ignoreCache: true
});
// Ajax URL
var ajax_url = "tmp/ajax.php";

// Offline Data Check Point
var db = openDatabase('views_db', '1.0', 'KhbSales Views DB', 2 * 1024 * 1024);
var dataSaved = true;
$(window).on('beforeunload', function () {
    if (!dataSaved) {
        session_check();
        return 'There is some data which is not synchronized to the server.\nIf you navigate away the data will lost.';
    }
});
function session_check(timeout) {
    var always = timeout === true ? true : false;
    //alert(always);
    timeout = typeof timeout === 'number' && timeout > 999 ? timeout : 5000;
    //alert(timeout);
    db.transaction(function (tx) {
        var views_arr = [];
        tx.executeSql('SELECT * FROM views', [], function (tx, results) {
            for (var i = 0; i < results.rows.length; i++) {
                var rows = results.rows.item(i);
                var options = {};
                options.session_id = rows.session_id;
                options.buyer_id = rows.buyer_id;
                options.lot_id = rows.lot_id;
                options.view = rows.view;
                options.spotter = rows.spotter;
                options.vet = rows.vet;
                views_arr.push(options);
            }
            if (results.rows.length > 0) {
                $.post(ajax_url, {act: 'store_offline_data', views_arr: views_arr}, function (data) {
                    if (data === 'success') {
                        db.transaction(function (tx) {
                            tx.executeSql('DELETE FROM views');
                            dataSaved = true;
                        });
                        if (always) {
                            setTimeout(function () {
                                session_check(true);
                            }, timeout);
                        }
                    }
                });
            } else {
                if (always) {
                    setTimeout(function () {
                        session_check(true);
                    }, timeout);
                }
            }
        });
    });
}
//session_check(10000);
// Page Init
$$(document).on('pageInit', function (x) {
    session_check();
    var calendarDefault = myApp.calendar({
        input: '#calendar-default'
    });
    var page = x.detail.page;
    $(document).ready(function () {
        $.post(ajax_url, {act: 'notif_message'}, function (data) {
            $('#show_notification_message').html(data);
        });
    });
    // Buyers JS
    if (page.name === 'buyerlist' || page.name === 'buyerinsert' || page.name === 'buyerupdate') {
        session_check();
        $(document).on('click', '#submit_buyer_btn,#update_buyer_btn', function (e) {
            myApp.showIndicator();
            $('.form_input').css('border', '1px solid #ddd');
            var email = $('input[name="email"]').val();
            var dataArray = {};
            dataArray.first_name = $('input[name="first_name"]').val();
            dataArray.last_name = $('input[name="last_name"]').val();
            var error = false;
            for (var i in dataArray) {
                if (!dataArray[i]) {
                    error = true;
                }
            }
            if (error === true) {
                e.preventDefault();
                $('.form_input').keyup();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
            if (!validateEmail(email)) {
                e.preventDefault();
                $('input[name="email"]').css('border', '1px solid red').focus();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
        }).on('blur', '.form_input', function () {
            $(this).keyup();
        }).on('keyup', '.form_input', function () {
            if ($(this).attr('data-skip') !== 'true') {
                if ($(this).val()) {
                    $(this).css('border', '1px solid #ddd');
                } else {
                    $(this).css('border', '1px solid red').focus();
                }
            }
        });
    }

    // Lots JS
    if (page.name === 'horselist' || page.name === 'horseinsert' || page.name === 'horseupdate') {
        session_check();
        $('#sale_head_cat').change(function (e) {
            e.preventDefault();
            var sale_head_id = $(this).val();
            $('#horse_data').html('<center><img src="assets/images/ajax-loading.gif"/></center>');
            $.post(ajax_url, {act: 'get_horse_list', sale_head_id: sale_head_id}, function (data) {
                $('#horse_data').html(data);
            });
        }).ready(function () {
            $('#sale_head_cat').change();
        });

        $(document).on('click', '#submit_lot_btn,#update_lot_btn', function (e) {
            myApp.showIndicator();
            $('.form_input').css('border', '1px solid #ddd');
            var dataArray = {};
            dataArray.sale_head_id = $('select[name="sale_head_id"]').val();
            dataArray.name = $('input[name="name"]').val();
            dataArray.lot_number = $('input[name="lot_number"]').val();
            dataArray.sire = $('input[name="sire"]').val();
            dataArray.dam = $('input[name="dam"]').val();
            dataArray.age = $('input[name="age"]').val();
            dataArray.sex = $('select[name="sex"]').val();
            dataArray.status = $('select[name="status"]').val();
            var error = false;
            for (var i in dataArray) {
                if (!dataArray[i]) {
                    error = true;
                }
            }
            if (error === true) {
                e.preventDefault();
                $('.form_input').keyup();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
        }).on('blur', '.form_input', function () {
            $(this).keyup();
        }).on('keyup', '.form_input', function () {
            if ($(this).attr('data-skip') !== 'true') {
                if ($(this).val()) {
                    $(this).css('border', '1px solid #ddd');
                } else {
                    $(this).css('border', '1px solid red').focus();
                }
            }
        });
    }

    // Results JS
    if (page.name === 'salelist' || page.name === 'saleinsert' || page.name === 'saleupdate') {
        session_check();
        $('#sale_results').change(function (e) {
            e.preventDefault();
            var sale_head_id = $(this).val();
            $('#results_data').html('<center><img src="assets/images/ajax-loading.gif"/></center>');
            $.post(ajax_url, {act: 'get_sale_results', sale_head_id: sale_head_id}, function (data) {
                $('#results_data').html(data);
            });
        }).ready(function () {
            $('#sale_results').change();
        });

        $('#sale_head_cat_result').change(function (e) {
            e.preventDefault();
            var sale_head_id = $(this).val();
            $('#lots_data').html('<center><img src="assets/images/ajax-loading.gif"/></center>');
            $.post(ajax_url, {act: 'get_lot_list_results', sale_head_id: sale_head_id}, function (data) {
                $('#lots_data').html(data);
            });
        }).ready(function () {
            $('#sale_head_cat_result').change();
        });

        $(document).on('click', '#submit_result_btn,#update_result_btn', function (e) {
            myApp.showIndicator();
            $('.form_input').css('border', '1px solid #ddd');
            var dataArray = {};
            dataArray.sale_head_id = $('select[name="sale_head_id"]').val();
            dataArray.buyer_id = $('select[name="lot_id"]').val();
            dataArray.buyer_id = $('select[name="buyer_id"]').val();
            dataArray.amount = $('input[name="amount"]').val();
            dataArray.date_add = $('input[name="date_add"]').val();
            var error = false;
            for (var i in dataArray) {
                if (!dataArray[i]) {
                    error = true;
                }
            }
            if (error === true) {
                e.preventDefault();
                $('.form_input').keyup();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
        }).on('blur', '.form_input', function () {
            $(this).keyup();
        }).on('keyup', '.form_input', function () {
            if ($(this).attr('data-skip') !== 'true') {
                if ($(this).val()) {
                    $(this).css('border', '1px solid #ddd');
                } else {
                    if ($(this).prop('tagName') === 'SELECT') {
                        $(this).css('border', '1px solid red');
                        $(".page-content").animate({
                            scrollTop: 80
                        }, "slow");
                        return;
                    } else {
                        $(this).css('border', '1px solid red').focus();
                    }
                }
            }
        });
    }

    // Sale Head JS
    if (page.name === 'sale_headlist' || page.name === 'sale_headinsert' || page.name === 'sale_headupdate') {
        session_check();
        $(document).on('click', '#submit_sale_head_btn,#update_sale_head_btn', function (e) {
            myApp.showIndicator();
            $('.form_input').css('border', '1px solid #ddd');
            var dataArray = {};
            dataArray.first_name = $('input[name="title"]').val();
            var error = false;
            for (var i in dataArray) {
                if (!dataArray[i]) {
                    error = true;
                }
            }
            if (error === true) {
                e.preventDefault();
                $('.form_input').keyup();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
        }).on('blur', '.form_input', function () {
            $(this).keyup();
        }).on('keyup', '.form_input', function () {
            if ($(this).attr('data-skip') !== 'true') {
                if ($(this).val()) {
                    $(this).css('border', '1px solid #ddd');
                } else {
                    $(this).css('border', '1px solid red').focus();
                }
            }
        });
    }

    // Setting JS
    if (page.name === 'settinglist' || page.name === 'settinginsert' || page.name === 'settingupdate') {
        session_check();
        $(document).on('click', '#submit_setting_btn', function (e) {
            myApp.showIndicator();
            $('.form_input').css('border', '1px solid #ddd');
            var dataArray = {};
            dataArray.email = $('input[name="email"]').val();
            dataArray.pass1 = $('input[name="password"]').val();
            dataArray.pass2 = $('input[name="password2"]').val();
            var error = false;
            for (var i in dataArray) {
                if (!dataArray[i]) {
                    error = true;
                }
            }
            if (error === true) {
                e.preventDefault();
                $('.form_input').keyup();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
            if (!validateEmail(dataArray.email)) {
                e.preventDefault();
                $('input[name="email"]').css('border', '1px solid red').focus();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
            if (dataArray.pass1 !== dataArray.pass2) {
                e.preventDefault();
                $('input[name="password2"]').css('border', '1px solid red');
                $('input[name="password"]').css('border', '1px solid red').focus();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
        }).on('blur', '.form_input', function () {
            $(this).keyup();
        }).on('keyup', '.form_input', function () {
            if ($(this).attr('data-skip') !== 'true') {
                if ($(this).val()) {
                    $(this).css('border', '1px solid #ddd');
                } else {
                    $(this).css('border', '1px solid red').focus();
                }
            }
        });
    }

    // User JS
    if (page.name === 'userlist' || page.name === 'userinsert' || page.name === 'userupdate') {
        session_check();
        $(document).on('click', '#submit_user_btn,#update_user_btn', function (e) {
            myApp.showIndicator();
            $('.form_input').css('border', '1px solid #ddd');
            var dataArray = {};
            dataArray.first_name = $('input[name="first_name"]').val();
            dataArray.last_name = $('input[name="last_name"]').val();
            dataArray.username = $('input[name="username"]').val();
            dataArray.pass1 = $('input[name="password"]').val();
            dataArray.pass2 = $('input[name="password2"]').val();
            dataArray.email = $('input[name="email"]').val();
            dataArray.phone = $('input[name="phone"]').val();
            var error = false;
            for (var i in dataArray) {
                if (!dataArray[i]) {
                    error = true;
                }
            }
            if (error === true) {
                e.preventDefault();
                $('.form_input').keyup();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
            if (dataArray.pass1 !== dataArray.pass2) {
                e.preventDefault();
                $('input[name="password2"]').css('border', '1px solid red');
                $('input[name="password"]').css('border', '1px solid red').focus();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
            if (!validateEmail(dataArray.email)) {
                e.preventDefault();
                $('input[name="email"]').css('border', '1px solid red').focus();
                setTimeout(function () {
                    myApp.hideIndicator();
                }, 300);
                return;
            }
        }).on('blur', '.form_input', function () {
            $(this).keyup();
        }).on('keyup', '.form_input', function () {
            if ($(this).attr('data-skip') !== 'true') {
                if ($(this).val()) {
                    $(this).css('border', '1px solid #ddd');
                } else {
                    $(this).css('border', '1px solid red').focus();
                }
            }
        });
    }

    // Reports JS
    if (page.name === 'reportlist' || page.name === 'reportinsert' || page.name === 'reportupdate') {
        session_check();
        function demoFromHTML(file_name) {
            var pdf = new jsPDF('1', 'pt', 'a4');
            source = $('#' + file_name)[0];
            specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };
            margins = {
                top: 10,
                bottom: 40,
                left: 40,
                right: 40,
                width: 522
            };
            pdf.fromHTML(
                    source,
                    margins.left,
                    margins.top, {
                        'width': margins.width,
                        'elementHandlers': specialElementHandlers
                    },
            function (dispose) {
                pdf.save(file_name + '.pdf');
            }, margins);
        }

        $(document).on('click', '#all_lots_total_views_report', function (e) {
            e.preventDefault();
            $('#all_reports_list').hide();
            $('#reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $('#back_to_reports').fadeIn();
            $.post(ajax_url, {act: 'all_lots_total_views_report'}, function (data) {
                $('#reports_data').html(data).slideDown();
                $('#sale_head_cat_reports').change();
            });
        });

        $(document).on('click', '#lots_buyers_report', function (e) {
            e.preventDefault();
            $('#all_reports_list').hide();
            $('#reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $('#back_to_reports').fadeIn();
            $.post(ajax_url, {act: 'lots_buyers_report'}, function (data) {
                $('#reports_data').html(data).slideDown();
                $('#sale_head_buyers_reports').change();
            });
        });


        $(document).on('click', '#lots_report', function (e) {
            e.preventDefault();
            $('#all_reports_list').hide();
            $('#reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $('#back_to_reports').fadeIn();
            $.post(ajax_url, {act: 'lots_report'}, function (data) {
                $('#reports_data').html(data).slideDown();
                $('#sale_head_cat_single_lot_report').change();
            });
        });


        $(document).on('click', '#all_lots_report', function (e) {
            e.preventDefault();
            $('#all_reports_list').hide();
            $('#reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $('#back_to_reports').fadeIn();
            $.post(ajax_url, {act: 'all_lots_report'}, function (data) {
                $('#reports_data').html(data).slideDown();
                $('#sale_head_cat_all_lots_reports').change();
            });
        });


        $(document).on('change', '#sale_head_cat_reports', function () {
            var sale_head_id = $(this).val();
            $('#horse_reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_horse_reports_data', sale_head_id: sale_head_id}, function (data) {
                $('#horse_reports_data').html(data).slideDown();
            });
        });

        $(document).on('change', '#sale_head_buyers_reports', function () {
            var sale_head_id = $(this).val();
            $('#horse_reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_horse_lots_buyers_report_data', sale_head_id: sale_head_id}, function (data) {
                $('#horse_reports_data').html(data).slideDown();
            });
        });


        $(document).on('change', '#sale_head_cat_single_lot_report', function () {
            var sale_head_id = $(this).val();
            $('#horse_reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_horse_lot_single_lot_report_data', sale_head_id: sale_head_id}, function (data) {
                $('#horse_reports_data').html(data).slideDown();
                $('#single_lot_report_data').change();
            });
        });

        $(document).on('change', '#single_lot_report_data', function () {
            var lot_id = $(this).val();
            var sale_head_id = $(this).attr('data-sale_head_id');
            $('#lot_reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_single_lot_report_data', lot_id: lot_id, sale_head_id: sale_head_id}, function (data) {
                $('#lot_reports_data').html(data).slideDown();
            });
        });
        $(document).on('change', '#sale_head_cat_all_lots_reports', function () {
            var sale_head_id = $(this).val();
            $('#horse_reports_data').html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_horse_all_lots_reports_data', sale_head_id: sale_head_id}, function (data) {
                $('#horse_reports_data').html(data).slideDown();
            });
        });

        // Get_All_Lots_view_date
        $(document).on('click', '#get_All_Lots_view_date', function (e) {
            e.preventDefault();
            var horse_id = $(this).attr('data-horse_id');
            $('.All_Lots_view_date_' + horse_id).html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_All_Lots_view_date', horse_id: horse_id}, function (data) {
                $('.All_Lots_view_date_' + horse_id).html(data).slideDown();
            });
        });

        // Get_All_Lots_By_Buyers_spotter_view_date
        $(document).on('click', '#get_All_Lots_By_Buyers_spotter_view_date', function (e) {
            e.preventDefault();
            var buyer_id = $(this).attr('data-buyer_id');
            var horse_id = $(this).attr('data-horse_id');
            $('.All_Lots_By_Buyers_spotter_view_date_' + horse_id + buyer_id).html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_All_Lots_By_Buyers_spotter_view_date', buyer_id: buyer_id, horse_id: horse_id}, function (data) {
                $('.All_Lots_By_Buyers_spotter_view_date_' + horse_id + buyer_id).html(data).slideDown();
            });
        });

        // Get_All_Lots_By_Buyers_vet_view_date
        $(document).on('click', '#get_All_Lots_By_Buyers_vet_view_date', function (e) {
            e.preventDefault();
            var buyer_id = $(this).attr('data-buyer_id');
            var horse_id = $(this).attr('data-horse_id');
            $('.All_Lots_By_Buyers_vet_view_date_' + horse_id + buyer_id).html('<div style="margin-top: 5%"><center><img src="assets/images/ajax-loading.gif"/></center></div>');
            $.post(ajax_url, {act: 'get_All_Lots_By_Buyers_vet_view_date', buyer_id: buyer_id, horse_id: horse_id}, function (data) {
                $('.All_Lots_By_Buyers_vet_view_date_' + horse_id + buyer_id).html(data).slideDown();
            });
        });

        $(document).on('click', '#back_to_reports', function (e) {
            e.preventDefault();
            $('#all_reports_list').slideDown();
            $('#reports_data').hide();
            $('#back_to_reports').fadeOut();
        });

        $(document).on('click', '#download_pdf', function () {
            var file_name = $(this).attr('data-div');
            demoFromHTML(file_name);
        });

        $(document).on('click', '#email_pdf', function () {
            myApp.showIndicator();
            var user_id = $(this).attr('data-user_id');
            var file_name = $(this).attr('data-div');
            var html = $('#' + file_name).html();
            $.post(ajax_url, {act: 'email_pdf_file', user_id: user_id, file_name: file_name, html: html}, function (data) {
                myApp.hideIndicator();
                if (data === 'success') {
                    toastr.success('Email Sent Successfully!', 'Success');
                }
            });
        });
    }
});
myApp.onPageInit('viewview', function () {
    session_check();
    $(document).on('click', '#cancel_buyer_submit', function (e) {
        e.preventDefault();
        mainView.router.back();
    });
    $(document).on('click', '#add_buyer_btn', function (e) {
        toastr.remove();
        myApp.showIndicator();
        $('.form_input').css('border', '1px solid #ddd');
        var email = $('input[name="email"]').val();
        var lots_array = [];
        $("input[name='lots[]']:checked").each(function ()
        {
            lots_array.push(Number($(this).val()));
        });
        var lots = lots_array.join();
        var dataArray = {};
        dataArray.first_name = $('input[name="first_name"]').val();
        dataArray.last_name = $('input[name="last_name"]').val();
        var error = false;
        for (var i in dataArray) {
            if (!dataArray[i]) {
                error = true;
            }
        }
        if (error === true) {
            e.preventDefault();
            $('.form_input').keyup();
            setTimeout(function () {
                myApp.hideIndicator();
            }, 300);
            return;
        }
        if (!validateEmail(email)) {
            e.preventDefault();
            $('input[name="email"]').css('border', '1px solid red').focus();
            setTimeout(function () {
                myApp.hideIndicator();
            }, 300);
            return;
        }
        if (error === false) {
            $.post(ajax_url, {act: 'save_buyer_details', dataArray: dataArray}, function (data) {
                if (data) {
                    if (!localStorage.buyers) {
                        localStorage.buyers = JSON.stringify([]);
                    }
                    var buyer = {};
                    buyer.id = data;
                    buyer.lots = lots;
                    if (localStorage !== undefined) {
                        if (!localStorage.buyers) {
                            localStorage.buyers = JSON.stringify([]);
                        } else {
                            var buyers = JSON.parse(localStorage.buyers);
                            buyers.push(buyer);
                            localStorage.buyers = JSON.stringify(buyers);
                        }
                    }
                    mainView.router.back();
                    toastr.success('Buyer Added Successfully!', 'Success');
                    $('#get_all_buyers_data').click();
                    myApp.hideIndicator();
                }
            });
        }
    }).on('blur', '.form_input', function () {
        $(this).keyup();
    }).on('keyup', '.form_input', function () {
        if ($(this).attr('data-skip') !== 'true') {
            if ($(this).val()) {
                $(this).css('border', '1px solid #ddd');
            } else {
                $(this).css('border', '1px solid red').focus();
            }
        }
    });
});
// User Side JS
$$(document).on('pageInit', '.page[data-page="viewlist"]', function (e) {
    setTimeout(function () {
        session_check(true);
    }, 5000);
    db.transaction(function (tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS views (session_id, buyer_id, lot_id, view, spotter, vet)');
    });

    $(document).on('click', '#get_all_buyers_data', function () {
        $('#list_all_buyers').html('<center><img src="assets/images/ajax-loading.gif"/></center>');
        $.post(ajax_url, {act: 'list_all_buyers'}, function (data) {
            $('#list_all_buyers').html(data);
        });
    });
    $('#lot_buyer_submit').click(function (e) {
        e.preventDefault();
        localStorage.removeItem('buyers');
        var sale_head_id = $('#sale_head_id_details').val();
        var link = $('#add_buyer_link').attr('href') + '&sale_head_id=' + sale_head_id;
        $('#add_buyer_link').show().attr('href', link);
        $('#lots_buyers_data').html('<center><img src="assets/images/ajax-loading.gif"/></center>');
        $('#reload_window').show();
        $('#sale_head_html_details').remove();
        $.post(ajax_url, {act: 'get_lots_buyers_results', sale_head_id: sale_head_id}, function (data) {
            $('#lots_buyers_data').html(data).slideDown();
            $('#get_all_buyers_data').click();
            $('#go_select_buyers').click();
            var mySearchbar = myApp.searchbar('.searchbar', {
                searchList: '.list-block-search',
                searchIn: '.item-title'
            });
            mySearchbar.enable();
            $(".chosen").data("placeholder", "Select Buyer...").chosen({
                disable_search_threshold: 1
            });
            $('#buyer_lots_done').click(function (e) {
                e.preventDefault();
                var buyer_id = $(this).attr('data-buyer_id');
                var lots_array = [];
                $("input[name='lots[]']:checked").each(function ()
                {
                    lots_array.push(Number($(this).val()));
                });
                var lots = lots_array.join();
                if (localStorage !== undefined) {
                    if (!localStorage.buyers) {
                        localStorage.buyers = JSON.stringify([]);
                    }
                    var buyer = {};
                    buyer.id = buyer_id;
                    buyer.lots = lots;
                    var buyers = JSON.parse(localStorage.buyers);
                    var new_buyers = [];
                    var buyer_found = false;
                    $.each(buyers, function (l, v) {
                        if (v.id === buyer_id) {
                            v.lots = lots;
                            buyer_found = true;
                        }
                        new_buyers.push(v);
                    });
                    if (!buyer_found) {
                        new_buyers.push(buyer);
                    }
                    localStorage.buyers = JSON.stringify(new_buyers);
                }
                $('input[name="lots[]"]:checked').removeAttr('checked');
                $('#accdetail_' + buyer_id).find('#lot_list li').each(function () {
                    var el = $(this);
                    var lot_id = Number(el.attr('data-lot_id'));
                    if (($.inArray(lot_id, lots_array)) === -1) {
                        el.remove();
                    }
                });
                $(this).unbind('click');
            });
        });
    });
    $(document).on('change', 'input[name="buyer_id[]"]', function (e) {
        e.preventDefault();
        var buyer_id = $(this).val();
        $('.buyers-picker-info').find('.close-picker').attr('data-buyer_id', buyer_id);
        if ($(this).is(":checked")) {
            myApp.pickerModal('.buyers-picker-info');
        }
        var sale_head_id = $('ul.select_buyers_data').attr('data-sale_head_id');
        var session_id = $('ul.select_buyers_data').attr('data-session_id');
        var buyers_lots = '';
        if (localStorage.buyers) {
            buyers_lots = $.parseJSON(localStorage.buyers);
            if ($(this).is(":checked")) {
                var new_buyers = [];
                $.each(buyers_lots, function (l, v) {
                    if (v.id !== buyer_id) {
                        new_buyers.push(v);
                    }
                });
                buyers_lots = new_buyers;
            }
        } else {
            buyers_lots = [];
        }
        var buyers = [];
        $('input[name="buyer_id[]"]:checked').each(function () {
            var options = {};
            options.buyer_id = $(this).val();
            options.buyer_name = $(this).attr('data-name');
            buyers.push(options);
        });
        $('#horses_list_buyers_options_data').html('<center><img src="assets/images/ajax-loading.gif"/></center>');
        $.post(ajax_url, {act: 'list_all_buyers', prev_buyers: buyers}, function (data) {
            $('#list_all_buyers').html(data);
        });
        $.post(ajax_url, {act: 'get_buyers_and_horses', buyers: buyers, sale_head_id: sale_head_id, session_id: session_id, buyers_lots: buyers_lots}, function (data) {
            $('#horses_list_buyers_options_data').html(data).slideDown(2000);
        });
    });
    // Offline Data Manuplation


    // Online Data Manuplation
    $(document).on('click', 'a#view_yes', function (e) {
        e.preventDefault();
        myApp.showIndicator();
        var el = $(this);
        var view_array = {};
        var buttons_row = el.closest('.buttons-row');
        view_array.session_id = $(this).attr('data-session_id');
        view_array.value = $(this).attr('data-value');
        view_array.buyer_id = $(this).attr('data-buyer_id');
        view_array.lot_id = $(this).attr('data-lot_id');
        el.attr('data-value', 'yes');
        buttons_row.find('a#view_spotter').attr('data-value', 'no');
        buttons_row.find('a#view_spotter').removeClass('label_success');
        buttons_row.find('a#view_vet').attr('data-value', 'no');
        buttons_row.find('a#view_vet').removeClass('label_success');
        el.addClass('label_success');
        myApp.hideIndicator();
    });
    $(document).on('click', 'a#view_no', function (e) {
        e.preventDefault();
        myApp.showIndicator();
        var section_remove_array = {};
        section_remove_array.session_id = $(this).attr('data-session_id');
        section_remove_array.buyer_id = $(this).attr('data-buyer_id');
        section_remove_array.lot_id = $(this).attr('data-lot_id');
        var class_detail = section_remove_array.session_id + section_remove_array.buyer_id + section_remove_array.lot_id;
        $.post(ajax_url, {act: 'remove_view_data', section_remove_array: section_remove_array}, function () {
            $('.li' + class_detail).fadeOut(400, function () {
                $(this).remove();
            });
            myApp.hideIndicator();
        });
    });

    $(document).on('click', 'a#view_spotter', function (e) {
        e.preventDefault();
        myApp.showIndicator();
        var el = $(this);
        var buttons_row = el.closest('.buttons-row');
        var spotter_array = {};
        spotter_array.session_id = $(this).attr('data-session_id');
        spotter_array.value = $(this).attr('data-value');
        spotter_array.buyer_id = $(this).attr('data-buyer_id');
        spotter_array.lot_id = $(this).attr('data-lot_id');
        el.attr('data-value', 'yes');
        buttons_row.find('a#view_yes').attr('data-value', 'no');
        buttons_row.find('a#view_yes').removeClass('label_success');
        buttons_row.find('a#view_vet').attr('data-value', 'no');
        buttons_row.find('a#view_vet').removeClass('label_success');
        el.addClass('label_success');
        myApp.hideIndicator();
    });
    $(document).on('click', 'a#view_vet', function (e) {
        e.preventDefault();
        myApp.showIndicator();
        var el = $(this);
        var buttons_row = el.closest('.buttons-row');
        var vet_array = {};
        vet_array.session_id = $(this).attr('data-session_id');
        vet_array.value = $(this).attr('data-value');
        vet_array.buyer_id = $(this).attr('data-buyer_id');
        vet_array.lot_id = $(this).attr('data-lot_id');
        el.attr('data-value', 'yes');
        buttons_row.find('a#view_yes').attr('data-value', 'no');
        buttons_row.find('a#view_yes').removeClass('label_success');
        buttons_row.find('a#view_spotter').attr('data-value', 'no');
        buttons_row.find('a#view_spotter').removeClass('label_success');
        el.addClass('label_success');
        myApp.hideIndicator();
    });

    $(document).on('click', 'a.clear_data', function (e) {
        e.preventDefault();
        //myApp.showIndicator();
        var el = $(this);
        var buttons_row = el.closest('.buttons-row');
        var view = buttons_row.find('a#view_yes').attr('data-value');
        var view_spotter = buttons_row.find('a#view_spotter').attr('data-value');
        var view_vet = buttons_row.find('a#view_vet').attr('data-value');
        var section_remove_array = {};
        section_remove_array.session_id = $(this).attr('data-session_id');
        section_remove_array.buyer_id = $(this).attr('data-buyer_id');
        section_remove_array.lot_id = $(this).attr('data-lot_id');
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO views (session_id, buyer_id, lot_id, view, spotter, vet) VALUES (?, ?, ?, ?, ?, ?)', [section_remove_array.session_id, section_remove_array.buyer_id, section_remove_array.lot_id, view, view_spotter, view_vet]);
            dataSaved = false;
        });
        var class_detail = section_remove_array.session_id + section_remove_array.buyer_id + section_remove_array.lot_id;
//        $.post(ajax_url, {act: 'save_total_view', view: view, view_spotter: view_spotter, view_vet: view_vet, section_remove_array: section_remove_array}, function () {
        var html = '';
        if (view === 'yes') {
            html = '<center><div class="buttons-row"><button style="width: 50%; margin: auto" class="button tab-link label_success"><i class="fa fa-check"></i> VIEWED</button></div></center>';
        }
        if (view_spotter === 'yes') {
            html = '<center><div class="buttons-row"><button style="width: 50%; margin: auto" class="button tab-link label_success"><i class="fa fa-check"></i> SP</button></div></center>';
        }
        if (view_vet === 'yes') {
            html = '<center><div class="buttons-row"><button style="width: 50%; margin: auto" class="button tab-link label_success"><i class="fa fa-check"></i> VET</button></div></center>';
        }
        $('.div' + class_detail).html(html);
//            myApp.hideIndicator();
//        });
    });

    $(document).on('click', '#delete_view_detail_id', function (e) {
        e.preventDefault();
        var view_id = $(this).attr('data-view_id');
        myApp.confirm('Are You Sure You Want To Delete This Record?', function () {
            $.post(ajax_url, {act: 'remove_extra_buyers', view_id: view_id}, function (data) {
                if (data === 'success') {
                    $('#li_' + view_id).hide(400, function () {
                        $('#li_' + view_id).remove();
                    });
                }
            });
        });
    });

    $(document).on('click', '#remove_select_buyers', function () {
        $($(this).parents('#scrolltop')[0]).hide(400, function () {
            $(this).remove();
        });
    });

    // Views Accordian
    $(document).on('click', '.accordion-section-title', function (e) {
        e.preventDefault();
        var acc_id = $(this).attr('data-id');
        $('#accordion_' + acc_id).slideDown();
        $('[data-id="' + acc_id + '"]').addClass('hideAcc');
    });
    $(document).on('click', '.hideAcc', function (e) {
        e.preventDefault();
        var acc_id = $(this).attr('data-id');
        $('[data-id="' + acc_id + '"]').removeClass('hideAcc');
        $('#accordion_' + acc_id).slideUp();
    });
});

// User Login JS
$(document).on('click', '#user_form_submit', function (e) {
    $('input#username').css('border', '1px solid #45535d');
    $('input#password').css('border', '1px solid #45535d');
    e.preventDefault();
    toastr.remove();
    var username = $('#username').val();
    var password = $('#password').val();
    if (!username) {
        $('input#username').css('border', '1px solid red');
    }
    if (!password) {
        $('input#password').css('border', '1px solid red');
    }
    if (username && password) {
        myApp.showIndicator();
        $.post(ajax_url, {act: 'user_login', username: username, password: password}, function (data) {
            myApp.hideIndicator();
            if (data === 'success') {
                $.cookie('khb_username', username, {expires: 365, path: '/'});
                localStorage.khb_username = username;
                localStorage.khb_password = password;
                window.location.href = "admin.php";
            } else if (data === 'error') {
                toastr.error('Wrong Username or Password!', 'Error!');
            } else {
                toastr.error('Something Went Wrong!', 'Error!');
            }
        });
    }
});

// Functions
function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}