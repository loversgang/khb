<?php
$root = dirname(__DIR__);
require_once($root . "/include/config/config.php");
require_once($root . "/include/functionClass/class.php");
$function = array('url', 'cart', 'input', 'admin', 'users', 'gallery', 'database', 'url_rewrite');
include_functions($function);
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'notif_message') {
        display_message(1);
        $error_obj->errorShow();
    }
    if ($act == 'user_login') {
        if ($user = validate_user('admin_user', $_POST)):
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
//setcookie("khb_username", $user->username, time() + (365 * 24 * 60 * 60), "/");
            echo "success";
        else:
            echo "error";
        endif;
    }
    if ($act == 'get_horse_list') {
        include_once($root . "/include/functionClass/horseClass.php");
        $obj = new horse;
        $horses = $obj->listHorses($sale_head_id);
        if (!$horses) {
            ?>
            <div class="alert alert-danger">
                No Lot Added!<br/>
                <a href="<?php echo make_load_url('horse', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Lot.
            </div>
        <?php } else { ?>
            <ul class="features_list_detailed" style="padding: 0px;margin-top: -5px">
                <?php foreach ($horses as $horse) { ?>
                    <li>
                        <div class="feat_small_details">                                
                            <a href="<?php echo make_load_url('horse', 'update', 'update', 'id=' . $horse->id) ?>"><h4><?php echo $horse->name; ?></h4>
                                L.No.<?php echo $horse->lot_number; ?></a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <?php
        }
    }
    if ($act == 'get_lot_list_results') {
        include_once($root . "/include/functionClass/horseClass.php");
        $obj = new horse;
        $horses = $obj->listHorses($sale_head_id);
        ?>
        <label>Lot</label>              
        <select name="lot_id" class="form_select form_input">
            <?php foreach ($horses as $horse) { ?>
                <option value="<?php echo $horse->id; ?>"><?php echo $horse->lot_number; ?></option>
            <?php } ?>
        </select>           
        <?php
    }
    if ($act == 'get_sale_results') {
        include_once($root . "/include/functionClass/saleClass.php");
        $obj = new sale;
        $sales = $obj->listSale($sale_head_id);
        if (!$sales) {
            ?>
            <div class="alert alert-danger">
                No Result Added!<br/>
                <a href="<?php echo make_load_url('sale', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Result.
            </div>
        <?php } else { ?>
            <ul class="features_list_detailed">
                <?php foreach ($sales as $sale) { ?>
                    <li>
                        <div class="feat_small_details">
                            <a href="<?php echo make_load_url('sale', 'update', 'update', 'id=' . $sale->id) ?>"><h4>L.No.<?php echo $sale->lot_number ?> / <?php echo $sale->name ?></h4>
                                <font style="">&#163;<?php echo $sale->amount ?></font> &nbsp; <?php echo $sale->date_add ?></a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <?php
        }
    }
    if ($act == 'get_lots_buyers_results') {
        include_once($root . "/include/functionClass/horseClass.php");
        include_once($root . "/include/functionClass/buyerClass.php");

// Generate Unique String (say session_id)
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONOPQRSTUVWXYZ0123456789';
        $session_id = '';
        for ($i = 0; $i < 10; $i++) {
            $session_id .= $characters[rand(0, strlen($characters) - 1)];
        }
        $obj = new horse;
        $horses = $obj->listHorses($sale_head_id);
        ?>
        <div class="picker-modal buyers-picker-info">
            <div class="toolbar">
                <div class="toolbar-inner">
                    <div class="left" style="margin-left: 10px;color:#fff">Select Lots</div>
                    <div class="right" style="margin-right: 10px"><a data-buyer_id="" href="#" id="buyer_lots_done" class="close-picker">Done</a></div>
                </div>
            </div>
            <div class="picker-modal-inner">
                <!--<div class="content-block" style="overflow: hidden">-->
                <div style="max-height: 100%;overflow: auto">
                    <div class="list-block">
                        <ul>
                            <?php foreach ($horses as $horse) { ?>
                                <li style="margin:0">
                                    <label class="label-checkbox item-content" style="padding: 5px 5px 5px 10px">
                                        <input type="checkbox" name="lots[]" value="<?php echo $horse->id; ?>">
                                        <div class="item-media" style="margin-top: -15px;"><i class="icon icon-form-checkbox"></i></div>
                                        <div class="item-inner">
                                            <div class="item-title" style="font-size: 20px;float:left !important"><?php echo $horse->name; ?></div>
                                        </div>
                                    </label>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!--</div>-->
            </div>
        </div>
        <?php
        $obj = new buyer;
        $buyers = $obj->listBuyers();
        if (!$buyers) {
            ?>
            <div class="alert alert-danger">
                No Buyer Found!
            </div>
        <?php } else { ?>
            <div class="list-block accordion-list">
                <ul style="background: #ddd">
                    <li class="accordion-item" style="padding: 5px 5px;">
                        <a href="#" class="item-link item-content" style="background-color: #ddd;">
                            <div class="item-inner">
                                <div class="item-title" style="margin-top: 10px;">
                                    <b>Select Buyers</b>
                                </div>
                            </div>
                        </a>
                        <div class="accordion-item-content">
                            <form data-search-list=".list-block-search" data-search-in=".item-title" class="searchbar searchbar-init" style="margin-top: -8px;">
                                <div class="searchbar-input" style="background-color: #fff;height: auto;padding:3px">
                                    <input style="height:auto" type="search" placeholder="Search"><a href="#" class="searchbar-clear"></a>
                                </div><a href="#" class="searchbar-cancel">Cancel</a>
                            </form>
                            <div class="content-block searchbar-not-found">
                                <div class="content-block-inner">No Buyer Found..!</div>
                            </div>
                            <div class="list-block list-block-search searchbar-found contacts-block" style="margin-bottom: -15px;">
                                <ul style="background-color: #f7f7f8;height: 150px; overflow: auto;margin-bottom: 15px;" class="select_buyers_data" data-session_id="<?php echo $session_id; ?>" data-sale_head_id="<?php echo $sale_head_id; ?>">
                                    <div class="list-group">
                                        <i id="get_all_buyers_data"></i>
                                        <div id="list_all_buyers"></div>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </li> 
                </ul>
            </div>
            <div id="horses_list_buyers_options_data"></div>
            <?php
        }
    }
    if ($act == 'get_buyers_and_horses') {
        $temp = array();
        foreach ($buyers_lots as $buyer_lot) {
            $temp[$buyer_lot['id']] = explode(',', $buyer_lot['lots']);
        }
        $buyers_lots = $temp;
//        pr($buyers_lots);exit;
        include_once($root . "/include/functionClass/horseClass.php");
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new horse;
        $horses = $obj->listHorses($sale_head_id);
        $buyers = array_map("unserialize", array_unique(array_map("serialize", $buyers)));
        foreach ($buyers as $buyer) {
            ?>
            <div class="accordion" id="accdetail_<?php echo $buyer['buyer_id']; ?>">
                <div class="accordion-section">

                    <a href="#" class="accordion-section-title" data-id="<?php echo $buyer['buyer_id']; ?>">
                        <b>
                            <?php echo ucfirst($buyer['buyer_name']); ?>
                        </b>
                    </a>
                    <div style="clear:both"></div>
                    <div id="accordion_<?php echo $buyer['buyer_id']; ?>" class="accordion-section-content">
                        <div class="list-block contacts-block">
                            <div class="list-group">
                                <ul style="background: #f7f7f8" id="lot_list">
                                    <?php
                                    foreach ($horses as $horse) {
                                        $chk = view::checkViewExists($session_id, $buyer['buyer_id'], $horse->id);
                                        if (array_key_exists($buyer['buyer_id'], $buyers_lots) && !in_array($horse->id, $buyers_lots[$buyer['buyer_id']]))
                                            continue;
                                        ?>
                                        <li style="list-style: none;" class="li<?php echo $session_id . $buyer['buyer_id'] . $horse->id; ?>" data-lot_id="<?php echo $horse->id; ?>">
                                            <div class="col-100">
                                                <center>
                                                    <?php
                                                    $views_count = view::viewCount($horse->id);
                                                    if ($views_count > 0) {
                                                        ?>
                                                        <a href="<?php echo make_load_url('view', 'update', 'update', 'horse_id=' . $horse->id); ?>" id="see_buyers_views" class="link"><h4 style="color: #3b5998" class="h4class"><font color="#467ef6"><?php echo $horse->lot_number; ?></font> <?php echo ucfirst($horse->name); ?> <font color="#467ef6"><?php echo $horse->sex; ?></font></h4></a>
                                                    <?php } else { ?>
                                                        <h4 class="h4class"><font color="#404040"><?php echo $horse->lot_number; ?></font> <?php echo ucfirst($horse->name); ?> <font color="#404040"><?php echo $horse->sex; ?></font></h4>
                                                    <?php } ?>
                                                </center>
                                            </div>
                                            <div style="clear: both"></div>
                                            <?php if ($chk) { ?>
                                                <?php if ($chk->view == 'yes') { ?>
                                                <center><div class="buttons-row"><button style="width: 50%; margin: auto" class="button tab-link label_success"><i class="fa fa-check"></i> VIEWED</button></div></center>
                                            <?php } ?>
                                            <?php if ($chk->spotter == 'yes') { ?>
                                                <center><div class="buttons-row"><button style="width: 50%; margin: auto" class="button tab-link label_success"><i class="fa fa-check"></i> SP</button></div></center>
                                            <?php } ?>
                                            <?php if ($chk->vet == 'yes') { ?>
                                                <center><div class="buttons-row"><button style="width: 50%; margin: auto" class="button tab-link label_success"><i class="fa fa-check"></i> VET</button></div></center>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div class="col-100 div<?php echo $session_id . $buyer['buyer_id'] . $horse->id; ?>" style="margin-top: 5px;margin-bottom: 5px">
                                                <div class="buttons-row" style="margin-bottom: 20px">
                                                    <a href="#" data-session_id="<?php echo $session_id; ?>" data-buyer_id="<?php echo $buyer['buyer_id']; ?>" data-lot_id="<?php echo $horse->id; ?>" class="clear_data button tab-link" style="background-color: #3b5998"><i class="fa fa-eye fa-2x" style="margin-top: 5px;"></i></a>
                                                    <a style="display: none" id="view_yes" href="#" data-session_id="<?php echo $session_id; ?>" data-buyer_id="<?php echo $buyer['buyer_id']; ?>" data-lot_id="<?php echo $horse->id; ?>" data-value="yes" class="view_yes_<?php echo $session_id; ?><?php echo $buyer['buyer_id']; ?><?php echo $horse->id; ?> button tab-link 
                                                    <?php
                                                    if ($chk) {
                                                        echo $chk->view == 'yes' ? 'label_success' : '';
                                                    } else {
                                                        echo 'label_success';
                                                    }
                                                    ?>"><i class="fa fa-check fa-2x" style="margin-top: 5px;"></i></a>
                                                    <a id="view_spotter" href="#" data-session_id="<?php echo $session_id; ?>" data-buyer_id="<?php echo $buyer['buyer_id']; ?>" data-lot_id="<?php echo $horse->id; ?>" data-value="no" class="view_spotter_<?php echo $session_id; ?><?php echo $buyer['buyer_id']; ?><?php echo $horse->id; ?> button tab-link 
                                                    <?php
                                                    if ($chk) {
                                                        echo $chk->spotter == 'yes' ? 'label_success' : '';
                                                    }
                                                    ?>">SP</a>
                                                    <a id="view_vet" href="#" data-session_id="<?php echo $session_id; ?>" data-buyer_id="<?php echo $buyer['buyer_id']; ?>" data-lot_id="<?php echo $horse->id; ?>" data-value="no" class="view_view_<?php echo $session_id; ?><?php echo $buyer['buyer_id']; ?><?php echo $horse->id; ?> button tab-link 
                                                    <?php
                                                    if ($chk) {
                                                        echo $chk->vet == 'yes' ? 'label_success' : '';
                                                    }
                                                    ?>">VET</a>
                                                    <a id="view_no" href="#" data-session_id="<?php echo $session_id; ?>" data-buyer_id="<?php echo $buyer['buyer_id']; ?>" data-lot_id="<?php echo $horse->id; ?>" data-value="no" class="view_no_<?php echo $session_id; ?><?php echo $buyer['buyer_id']; ?><?php echo $horse->id; ?> button tab-link label_danger"><i class="fa fa-times fa-2x" style="margin-top: 5px;"></i></a>

                                                </div>
                                            </div>
                                        <?php } ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!--end .accordion-section-->
            </div>
            <?php
        }
    }
    if ($act == 'save_view') {
        include_once($root . "/include/functionClass/viewClass.php");
        $chk = view::checkViewExists($view_array['session_id'], $view_array['buyer_id'], $view_array['lot_id']);
        if (!$chk) {
            $view_array['user_id'] = $_SESSION['admin_session_secure']['user_id'];
            $view_array['view'] = $view_array['value'];
            $obj = new view;
            $obj->saveView($view_array);
        } else {
            $view_array['id'] = $chk->id;
            $view_array['view'] = $view_array['value'];
            $obj = new view;
            $obj->saveView($view_array);
        }
    }
    if ($act == 'remove_view') {
        include_once($root . "/include/functionClass/viewClass.php");
        $chk = view::checkViewExists($view_array['session_id'], $view_array['buyer_id'], $view_array['lot_id']);
        if (!$chk) {
            $view_array['user_id'] = $_SESSION['admin_session_secure']['user_id'];
            $obj = new view;
            $obj->saveView($view_array);
        } else {
            $view_array['id'] = $chk->id;
            $obj = new view;
            $obj->saveView($view_array);
        }
    }
    if ($act == 'save_spotter') {
        include_once($root . "/include/functionClass/viewClass.php");
        $chk = view::checkViewExists($spotter_array['session_id'], $spotter_array['buyer_id'], $spotter_array['lot_id']);
        if (!$chk) {
            $spotter_array['user_id'] = $_SESSION['admin_session_secure']['user_id'];
            $spotter_array['spotter'] = $spotter_array['value'];
            $obj = new view;
            $obj->saveView($spotter_array);
        } else {
            $spotter_array['id'] = $chk->id;
            $spotter_array['spotter'] = $spotter_array['value'];
            $obj = new view;
            $obj->saveView($spotter_array);
        }
    }
    if ($act == 'save_vet') {
        include_once($root . "/include/functionClass/viewClass.php");
        $chk = view::checkViewExists($vet_array['session_id'], $vet_array['buyer_id'], $vet_array['lot_id']);
        if (!$chk) {
            $vet_array['user_id'] = $_SESSION['admin_session_secure']['user_id'];
            $vet_array['vet'] = $vet_array['value'];
            $obj = new view;
            $obj->saveView($vet_array);
        } else {
            $vet_array['id'] = $chk->id;
            $vet_array['vet'] = $vet_array['value'];
            $obj = new view;
            $obj->saveView($vet_array);
        }
    }
    if ($act == 'save_total_view') {
        include_once($root . "/include/functionClass/viewClass.php");
        $chk = view::checkViewExists($section_remove_array['session_id'], $section_remove_array['buyer_id'], $section_remove_array['lot_id']);
        if (!$chk) {
            $vet_array['user_id'] = $_SESSION['admin_session_secure']['user_id'];
            $vet_array['session_id'] = $section_remove_array['session_id'];
            $vet_array['buyer_id'] = $section_remove_array['buyer_id'];
            $vet_array['lot_id'] = $section_remove_array['lot_id'];
            $vet_array['view'] = $view;
            $vet_array['spotter'] = $view_spotter;
            $vet_array['vet'] = $view_vet;
            $obj = new view;
            $obj->saveView($vet_array);
        } else {
            $vet_array['id'] = $chk->id;
            $vet_array['session_id'] = $section_remove_array['session_id'];
            $vet_array['buyer_id'] = $section_remove_array['buyer_id'];
            $vet_array['lot_id'] = $section_remove_array['lot_id'];
            $vet_array['view'] = $view;
            $vet_array['spotter'] = $view_spotter;
            $vet_array['vet'] = $view_vet;
            $obj = new view;
            $obj->saveView($vet_array);
        }
    }
    if ($act == 'store_offline_data') {
        ignore_user_abort(true);
        include_once($root . "/include/functionClass/viewClass.php");
        foreach ($views_arr as $section_remove_array) {
            $chk = view::checkViewExists($section_remove_array['session_id'], $section_remove_array['buyer_id'], $section_remove_array['lot_id']);
            if (!$chk) {
                $vet_array['user_id'] = $_SESSION['admin_session_secure']['user_id'];
                $vet_array['session_id'] = $section_remove_array['session_id'];
                $vet_array['buyer_id'] = $section_remove_array['buyer_id'];
                $vet_array['lot_id'] = $section_remove_array['lot_id'];
                $vet_array['view'] = $section_remove_array['view'];
                $vet_array['spotter'] = $section_remove_array['spotter'];
                $vet_array['vet'] = $section_remove_array['vet'];
                $obj = new view;
                $obj->saveView($vet_array);
            } else {
                $vet_array['id'] = $chk->id;
                $vet_array['session_id'] = $section_remove_array['session_id'];
                $vet_array['buyer_id'] = $section_remove_array['buyer_id'];
                $vet_array['lot_id'] = $section_remove_array['lot_id'];
                $vet_array['view'] = $section_remove_array['view'];
                $vet_array['spotter'] = $section_remove_array['spotter'];
                $vet_array['vet'] = $section_remove_array['vet'];
                $obj = new view;
                $obj->saveView($vet_array);
            }
        }
        echo 'success';
    }
    if ($act == 'remove_view_data') {
        include_once($root . "/include/functionClass/viewClass.php");
        $chk = view::checkViewExists($section_remove_array['session_id'], $section_remove_array['buyer_id'], $section_remove_array['lot_id']);
        if ($chk) {
            $obj = new view;
            $obj->id = $chk->id;
            $obj->Delete();
        }
    }

    if ($act == 'all_lots_total_views_report') {
        include_once($root . "/include/functionClass/saleClass.php");
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        if (!$sale_heads) {
            ?>
            <div class="alert alert-danger">
                No Sale Name Added!<br/>
                <a href="<?php echo make_load_url('sale_head', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Sale Names.
            </div>
        <?php } else { ?>
            <div class="contactform" style="text-align: center;margin-bottom: -10px;">
                <label>Select Sale Name</label>
                <select name="sale_head_id" class="form_select active-state" id="sale_head_cat_reports">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div id="horse_reports_data"></div>
            <?php
        }
    }

    if ($act == 'get_horse_reports_data') {
        include_once($root . "/include/functionClass/viewClass.php");
        $dates = view::getHorseViewDates();
        $obj = new view;
        $horse_reports = $obj->getLotsTotalViewsReport($sale_head_id);
        if (!$horse_reports) {
            ?>
            <div class="alert alert-danger">
                No Report Calculated..!
            </div>
        <?php } else { ?>
            <div id="All_Lots_And_Total_Views_Report">
                <div class="contactform" style="text-align: center">
                    <label>All Lots & Total Views Report</label>
                </div>
                <?php foreach ($dates as $date) { ?>
                    <div class="contactform" style="text-align: center;padding: 10px 0 0px 0;">
                        <label><?php echo date('d/m/Y', strtotime($date->date)); ?></label>
                    </div>
                    <?php
                    foreach ($horse_reports as $horse_report) {
                        $total_view_count_by_date = view::horsetotalViewCountByDate($horse_report->lot_id, $date->date);
                        if ($total_view_count_by_date == 0)
                            continue;
                        ?>
                        <table style="width: 100%;text-align: center;color: #000" class="views_table">
                            <thead>
                                <tr>
                                    <td>
                                        <b>
                                            <span style="font-size: 18px;color:green"><?php echo $horse_report->name ?> <?php echo $horse_report->sex ?></span> - <span style="font-size: 16px"><?php echo $total_view_count_by_date; ?> Views</span>
                                        </b>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total_views_by_date = view::getLotViewsByDate($horse_report->lot_id, $date->date);
                                foreach ($total_views_by_date as $horse_report) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d-m-Y H:i:s', strtotime($horse_report->date_viewed)); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                <?php } ?>
            </div>
            <div id="editor"></div>
            <div style="clear:both"></div>
            <div class="button button-big button-fill" data-div="All_Lots_And_Total_Views_Report" id="download_pdf">DOWNLOAD</div>
            <div class="button button-big button-fill" data-user_id="<?php echo $_SESSION['admin_session_secure']['user_id']; ?>" data-div="All_Lots_And_Total_Views_Report" id="email_pdf">EMAIL</div>
        <?php } ?>
        <?php
    }

    if ($act == 'lots_buyers_report') {
        include_once($root . "/include/functionClass/buyerClass.php");
        $obj = new buyer;
        $buyers = $obj->listBuyers();
        if (!$buyers) {
            ?>
            <div class="alert alert-danger">
                No Buyer Added!<br/>
                <a href="<?php echo make_load_url('buyer', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Buyer.
            </div>
        <?php } else { ?>
            <div class="contactform" style="text-align: center;margin-bottom: -10px;">
                <label>Select Buyer</label>
                <select name="buyer_id" class="form_select active-state" id="sale_head_cat_lots_buyers_report">
                    <?php foreach ($buyers as $buyer) { ?>
                        <option value="<?php echo $buyer->id; ?>"><?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?></option>
                    <?php } ?>
                </select>
            </div>
            <div id="horse_reports_data"></div>
            <?php
        }
    }

    if ($act == 'get_horse_lots_buyers_report_data') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $horses = $obj->getViewsByBuyerId($buyer_id);
        $dates = view::getHorseViewDatesByBuyerId($buyer_id);
        if (!$horses) {
            ?>
            <div class="alert alert-danger">
                No Report Calculated..!
            </div>
        <?php } else { ?>
            <div id="All_Lots_By_Buyers_Report">
                <div class="contactform" style="text-align: center">
                    <label>All Lots By Buyers Report</label>
                </div>
                <?php
                if (!$dates) {
                    ?>
                    <div class="alert alert-danger">
                        No Report Calculated..!
                    </div>
                    <?php
                }
                ?>
                <?php foreach ($dates as $date) { ?>
                    <div class="contactform" style="text-align: center;padding: 10px 0 0px 0;">
                        <label><?php echo date('d/m/Y', strtotime($date->date)); ?></label>
                    </div>
                    <?php
                    foreach ($horses as $horse_report) {
                        $total_view_count_by_date = view::horseBuyertotalViewCountByDate($horse_report->lot_id, $date->date, $buyer_id);
                        if ($total_view_count_by_date == 0)
                            continue;
                        ?>
                        <table style="width: 100%;text-align: center;color: #000" class="views_table">
                            <thead>
                                <tr>
                                    <td>
                                        <b>
                                            <span style="font-size: 18px;color:green"><?php echo $horse_report->name ?> (<?php echo $horse_report->lot_number ?>)</span><br/>
                                            <span style="font-size: 16px">Views (<?php echo $total_view_count_by_date; ?>)</span>
                                        </b>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total_views_by_date = view::getBuyerLotViewsByDate($horse_report->lot_id, $date->date, $buyer_id);
                                foreach ($total_views_by_date as $horse_report) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d-m-Y H:i:s', strtotime($horse_report->date_viewed)); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                <?php } ?>
            </div>
            <div id="editor"></div>
            <div style="clear:both"></div>
            <div class="button button-big button-fill" data-div="All_Lots_By_Buyers_Report" id="download_pdf">DOWNLOAD</div>
            <div class="button button-big button-fill" data-user_id="<?php echo $_SESSION['admin_session_secure']['user_id']; ?>" data-div="All_Lots_By_Buyers_Report" id="email_pdf">EMAIL</div>
        <?php } ?>
        <?php
    }

    if ($act == 'lots_report') {
        include_once($root . "/include/functionClass/saleClass.php");
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        if (!$sale_heads) {
            ?>
            <div class="alert alert-danger">
                No Sale Name Added!<br/>
                <a href="<?php echo make_load_url('sale_head', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Sale Names.
            </div>
        <?php } else { ?>
            <div class="contactform" style="text-align: center;margin-bottom: -10px;">
                <label>Select Sale Name</label>
                <select name="sale_head_id" class="form_select active-state" id="sale_head_cat_single_lot_report">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div id="horse_reports_data"></div>
            <?php
        }
    }
    if ($act == 'get_horse_lot_single_lot_report_data') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $horse_reports = $obj->getLotsTotalViewsReport($sale_head_id);
        if (!$horse_reports) {
            ?>
            <div class="alert alert-danger">
                No Report Calculated..!
            </div>
        <?php } else { ?>
            <div class="contactform" style="text-align: center;margin-bottom: -10px;">
                <label>Select Horse/Lot:</label>
                <select name="lot_id" data-sale_head_id="<?php echo $sale_head_id ?>" class="form_select active-state" id="single_lot_report_data">
                    <?php foreach ($horse_reports as $horse_report) { ?>
                        <option value="<?php echo $horse_report->lot_id; ?>"><?php echo $horse_report->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div id="horse_reports_data"></div>
            <div id="lot_reports_data"></div>
            <?php
        }
    }

    if ($act == 'get_single_lot_report_data') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $horse_report = $obj->getHorseTotalReport($sale_head_id, $lot_id);
        $object = new view;
        $buyers = $object->getBuyersBuHorseId($lot_id);
        $dates = view::getHorseViewDatesByHorseId($lot_id);
        if (!$horse_report) {
            ?>
            <div class="alert alert-danger">
                No Report Calculated..!
            </div>
        <?php } else { ?>
            <div id="Results_By_Lot_Report">
                <div class="contactform" style="text-align: center">
                    <label>All Views By Horse Report</label>
                </div>
                <div class="contactform" style="text-align: center">
                    <label><font color="red"><?php echo $horse_report->name ?></font></label>
                </div>
                <?php foreach ($dates as $date) { ?>
                    <div class="contactform" style="text-align: center;padding: 10px 0 0px 0;">
                        <label><?php echo date('l/M/Y', strtotime($date->date)); ?></label>
                    </div>
                    <?php
                    foreach ($buyers as $buyer) {
                        $total_view_count_by_date = view::horseBuyertotalViewCountByDate($horse_report->lot_id, $date->date, $buyer->id);
                        if ($total_view_count_by_date == 0)
                            continue;
                        ?>
                        <table style="width: 100%;text-align: center;color: #000" class="views_table">
                            <thead>
                                <tr>
                                    <td>
                                        <b>
                                            <span style="font-size: 18px;color:green"><?php echo $buyer->first_name ?> <?php echo $buyer->last_name ?></span><br/>
                                            <span style="font-size: 16px">Views (<?php echo $total_view_count_by_date; ?>)</span>
                                        </b>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total_views_by_date = view::getBuyerLotViewsByDate($horse_report->lot_id, $date->date, $buyer->id);
                                foreach ($total_views_by_date as $horse_report) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d-m-Y H:i:s', strtotime($horse_report->date_viewed)); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                <?php } ?>
            </div>
            <div id="editor"></div>
            <div style="clear:both"></div>
            <div class="button button-big button-fill" data-div="Results_By_Lot_Report" id="download_pdf">DOWNLOAD</div>
            <div class="button button-big button-fill" data-user_id="<?php echo $_SESSION['admin_session_secure']['user_id']; ?>" data-div="Results_By_Lot_Report" id="email_pdf">EMAIL</div>
        <?php } ?>
        <?php
    }

    if ($act == 'all_lots_report') {
        include_once($root . "/include/functionClass/saleClass.php");
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        if (!$sale_heads) {
            ?>
            <div class="alert alert-danger">
                No Sale Name Added!<br/>
                <a href="<?php echo make_load_url('sale_head', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Sale Names.
            </div>
        <?php } else { ?>
            <div class="contactform" style="text-align: center;margin-bottom: -10px;">
                <label>Select Sale Name</label>
                <select name="sale_head_id" class="form_select active-state" id="sale_head_cat_all_lots_reports">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div id="horse_reports_data"></div>
            <?php
        }
    }

    if ($act == 'get_horse_all_lots_reports_data') {
        include_once($root . "/include/functionClass/saleClass.php");
        $obj = new sale;
        $sales = $obj->getTotalSales($sale_head_id);
        if (!$sales) {
            ?>
            <div class="alert alert-danger">
                No Report Calculated..!
            </div>
        <?php } else { ?>
            <div id="Results_By_All_Lots_Report">
                <div class="contactform" style="text-align: center">
                    <label>Results By All Lots Report</label>
                </div>
                <table style="width: 100%;text-align: center;color: #000" class="views_table">
                    <thead>
                        <tr>
                            <td style="width: 20%"><b>Horse</b></td>
                            <td style="width: 20%"><b>Lot No.</b></td>
                            <td style="width: 20%"><b>Purchaser</b></td>
                            <td style="width: 20%"><b>Amount(&#163;)</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sales as $sale) { ?>
                            <tr>
                                <td style="width: 25%">
                                    <?php echo $sale->horse_name ?>
                                </td>
                                <td style="width: 20%">
                                    <?php echo $sale->lot_number ?>
                                </td>
                                <td style="width: 30%">
                                    <?php echo $sale->first_name ?> <?php echo $sale->last_name ?>
                                </td>
                                <td style="width: 25%">
                                    &#163; <?php echo $sale->amount ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div id="editor"></div>
            <div style="clear:both"></div>
            <div class="button button-big button-fill" data-div="Results_By_All_Lots_Report" id="download_pdf">DOWNLOAD</div>
            <div class="button button-big button-fill" data-user_id="<?php echo $_SESSION['admin_session_secure']['user_id']; ?>" data-div="Results_By_All_Lots_Report" id="email_pdf">EMAIL</div>
        <?php } ?>
        <?php
    }
    if ($act == 'email_pdf_file') {
        include_once($root . "/include/functionClass/adminUserClass.php");
        $obj = new admin;
        $user = $obj->getAdminUser($user_id);
        require_once $root . "/include/dompdf/dompdf/dompdf_config.inc.php";
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $pdf_content = $dompdf->output();
        require_once($root . "/include/dompdf/swift/swift_required.php");
        $subject = str_replace('_', ' ', $file_name);
        $mailer = new Swift_Mailer(new Swift_MailTransport());
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setTo(array($user->email => $user->first_name)) // Array of people to send to
                ->setFrom(array('rocky.developer004@gmail.com' => 'KHBSales')) // From:
                ->setBody($subject, 'text/html')
                ->attach(Swift_Attachment::newInstance($pdf_content, $file_name . '.pdf', 'application/pdf'));
        if ($mailer->send($message)) {
            $success = true;
            echo "success";
        } else {
            $error = true;
        }
    }

    if ($act == 'remove_extra_buyers') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $obj->id = $view_id;
        $obj->Delete();
        echo "success";
    }
    if ($act == 'save_buyer_details') {
        include_once($root . "/include/functionClass/buyerClass.php");
        $chk = buyer::chkBuyerExists($dataArray['first_name'], $dataArray['last_name']);
        if (!$chk) {
            $obj = new buyer;
            $buyer_id = $obj->saveBuyer($dataArray);
            unset($dataArray['first_name']);
            unset($dataArray['last_name']);
            echo $buyer_id;
        }
    }

    if ($act == 'list_all_buyers') {
        include_once($root . "/include/functionClass/buyerClass.php");
        $obj = new buyer;
        $buyers = $obj->listBuyers();
        $prev_buyer_ids = array();
        if (isset($prev_buyers)) {
            foreach ($prev_buyers as $prev_buyer) {
                $prev_buyer_ids[] = $prev_buyer['buyer_id'];
            }
            $temp = array();
            foreach ($buyers as $k => $buyer) {
                if (in_array($buyer->id, $prev_buyer_ids)) {
                    $temp[] = $buyer;
                    unset($buyers[$k]);
                }
            }
            $buyers = array_merge($temp, $buyers);
        }
        foreach ($buyers as $buyer) {
            $maybe_checked = in_array($buyer->id, $prev_buyer_ids) ? ' checked' : '';
            ?>
            <li>
                <label class="label-checkbox item-content" style="margin-bottom: 10px;">

                    <input name="buyer_id[]" data-name="<?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?>" type="checkbox" value="<?php echo $buyer->id; ?>"<?php echo $maybe_checked ?>>
                    <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
                    <div class="item-inner">
                        <div class="item-title"><?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?></div>
                    </div>
                </label>
            </li>
            <?php
        }
    }

    if ($act == 'get_All_Lots_By_Buyers_spotter_view_date') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $get_spotter_views_dates = $obj->get_views_date($buyer_id, $horse_id, 'spotter');
        $k = 1;
        foreach ($get_spotter_views_dates as $date) {
            ?>
            <b><?php echo $k++; ?>.</b> <?php echo $date->date_viewed; ?><br/>
            <?php
        }
    }
    if ($act == 'get_All_Lots_By_Buyers_vet_view_date') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $get_vet_views_dates = $obj->get_views_date($buyer_id, $horse_id, 'vet');
        $k = 1;
        foreach ($get_vet_views_dates as $date) {
            ?>
            <b><?php echo $k++; ?>.</b> <?php echo $date->date_viewed; ?><br/>
            <?php
        }
    }
    if ($act == 'get_All_Lots_view_date') {
        include_once($root . "/include/functionClass/viewClass.php");
        $obj = new view;
        $get_all_views_dates = $obj->get_horse_total_views_date($horse_id);
        $k = 1;
        foreach ($get_all_views_dates as $date) {
            ?>
            <b><?php echo $k++; ?>.</b> <?php echo $date->date_viewed; ?><br/>
            <?php
        }
    }
}    