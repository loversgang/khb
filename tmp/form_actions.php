<?php

if (!empty($_POST)) {
    $root = dirname(__DIR__);
    require_once($root . "/include/config/config.php");
    require_once($root . "/include/functionClass/class.php");
    $function = array('url', 'cart', 'input', 'admin', 'users', 'gallery', 'database', 'url_rewrite');
    include_functions($function);

    // Save Buyer
    if (isset($_POST['save_buyer']) || isset($_POST['update_buyer'])) {
        include_once($root . "/include/functionClass/buyerClass.php");
        $obj = new buyer;
        $obj->saveBuyer($_POST);
        if (isset($_POST['id'])) {
            $admin_user->set_pass_msg("Buyer Updated Successfully!");
        } else {
            $admin_user->set_pass_msg("Buyer Added Successfully!");
        }
    }


    // Save Lot
    if (isset($_POST['save_lot']) || isset($_POST['update_lot'])) {
        include_once($root . "/include/functionClass/horseClass.php");
        $obj = new horse;
        $obj->saveHorse($_POST);
        if (isset($_POST['id'])) {
            $admin_user->set_pass_msg("Lot Updated Successfully!");
        } else {
            $admin_user->set_pass_msg("Lot Added Successfully!");
        }
    }

    // Save Result
    if (isset($_POST['save_result']) || isset($_POST['update_result'])) {
        include_once($root . "/include/functionClass/saleClass.php");
        include_once($root . '/include/functionClass/horseClass.php');
        $obj = new sale;
        $obj->saveSale($_POST);
        if (isset($_POST['id'])) {
            $admin_user->set_pass_msg("Result Updated Successfully!");
        } else {
            $admin_user->set_pass_msg("Result Added Successfully!");
        }
        $arr['id'] = $_POST['lot_id'];
        $arr['status'] = "sold";
        $obj = new horse;
        $obj->saveHorse($arr);
    }

    // Save Sale Head
    if (isset($_POST['save_sale_head']) || isset($_POST['update_sale_head'])) {
        include_once($root . "/include/functionClass/saleClass.php");
        $obj = new sale_head;
        $obj->saveSaleHead($_POST);
        if (isset($_POST['id'])) {
            $admin_user->set_pass_msg("Sale Head Updated Successfully!");
        } else {
            $admin_user->set_pass_msg("Sale Head Added Successfully!");
        }
    }

    if (isset($_POST['save_setting'])) {
        include_once($root . "/include/functionClass/adminUserClass.php");
        $obj = new admin;
        $admin = $obj->getAdminUser($_SESSION['admin_session_secure']['user_id']);
        $chk_email = admin::checkFieldExists('email', $_POST['email'], $admin->id);
        if (!$chk_email) {
            $obj = new admin;
            $obj->saveAdminUser($_POST);
            $admin_user->set_pass_msg("Admin Details Updated Successfully!");
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Email Already Exists!");
        }
    }

    if (isset($_POST['save_user'])) {
        include_once($root . "/include/functionClass/adminUserClass.php");
        $chk_username = admin::checkFieldExists('username', $_POST['username']);
        $chk_email = admin::checkFieldExists('email', $_POST['email']);
        if ($chk_username) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Username Already Exists!");
        } elseif ($chk_email) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Email Already Exists!");
        } else {
            $obj = new admin;
            $obj->saveAdminUser($_POST);
            $admin_user->set_pass_msg("User Added Successfully!");
        }
    }

    if (isset($_POST['update_user'])) {
        include_once($root . "/include/functionClass/adminUserClass.php");
        $obj = new admin;
        $admin = $obj->getAdminUser($_POST['id']);
        $chk_username = admin::checkFieldExists('username', $_POST['username'], $admin->id);
        $chk_email = admin::checkFieldExists('email', $_POST['email'], $admin->id);
        if (!$chk_username) {
            if (!$chk_email) {
                $obj = new admin;
                $obj->saveAdminUser($_POST);
                $admin_user->set_pass_msg("User Updated Successfully!");
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg("Email Already Exists!");
            }
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Username Already Exists!");
        }
    }
}