<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <link rel="apple-touch-icon" href="assets/images/favicons/apple-touch-icon.png" />
        <link rel="icon" type="image/png" href="assets/images/favicons/favicon-96x96.png" sizes="96x96">
        <meta name="msapplication-TileImage" content="assets/images/favicons/mstile-144x144.png">
        <link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="apple-touch-startup-image-640x1096.png">
        <title>KHBSales</title>
        <link rel="shortcut icon" href="assets/images/favicons/favicon.png" />
        <link rel="stylesheet" href="assets/css/app_css/framework7.ios.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/plugins/toastr/toastr.css">
        <link rel="stylesheet" href="assets/css/colors/custom.css">
        <link type="text/css" rel="stylesheet" href="assets/plugins/fa-fa/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="assets/plugins/chosen/chosen.css" />
        <link type="text/css" rel="stylesheet" href="assets/plugins/indexed-list/framework7.indexed-list.css" />
        <script type="text/javascript" src="assets/plugins/jquery/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="assets/js/app_js/jquery.cookie.js"></script>
    </head>
    <!-- END HEAD -->