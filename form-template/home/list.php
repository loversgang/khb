<?php if ($_SESSION['admin_session_secure']['user_type'] == 'user') { ?>
    <div class="page-content" style="overflow: auto;padding-top: 20px;padding-right: 10px;padding-left: 10px;">
        <div class="list-block">
            <div class="row" style="margin-top: 60px;">
                <div class="col-100">
                    <div class="menu_content menu_content_user">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('view') ?>"><img src="assets/images/icons/white/sales.png" alt="" title="" />
                            <br/>
                            <span>Views</span>
                        </a>
                    </div>
                </div>
                <div class="col-100">
                    <div class="menu_content menu_content_user">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('report') ?>"><img src="assets/images/icons/white/report.png" alt="" title="" />
                            <br/>
                            <span>Reports</span>
                        </a>
                    </div>
                </div>
                <div class="col-100">
                    <div class="menu_content menu_content_user">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('about') ?>"><img src="assets/images/icons/white/about.png" alt="" title="" />
                            <br/>
                            <span>About</span>
                        </a>
                    </div>
                </div>
                <div class="col-100">
                    <div class="menu_content menu_content_user">
                        <a style="color: #fff;" class="item-link" onclick="location.href = '<?php echo make_admin_url('logout'); ?>'" style="cursor: pointer"><img src="assets/images/icons/white/lock.png" alt="" title="" />
                            <br/>
                            <span>Logout</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="page-content" style="overflow: auto;padding-top: 20px;padding-right: 10px;padding-left: 10px;">
        <div class="list-block">
            <div class="row" style="margin-top: 60px;">
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('buyer') ?>"><img src="assets/images/icons/white/buyer.png" alt="" title="" />
                            <br/>
                            <span>Buyers</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('sale_head') ?>"><img src="assets/images/icons/white/sale_head.png" alt="" title="" />
                            <br/>
                            <span>Sale Name</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('horse') ?>"><img src="assets/images/icons/white/lots.png" alt="" title="" />
                            <br/>
                            <span>Lots</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('sale') ?>"><img src="assets/images/icons/white/sales.png" alt="" title="" />
                            <br/>
                            <span>Results</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('user') ?>"><img src="assets/images/icons/white/team.png" alt="" title="" />
                            <br/>
                            <span>Users</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('report') ?>"><img src="assets/images/icons/white/report.png" alt="" title="" />
                            <br/>
                            <span>Reports</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('setting') ?>"><img src="assets/images/icons/white/settings.png" alt="" title="" />
                            <br/>
                            <span>Setting</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('view') ?>"><img src="assets/images/icons/white/sales.png" alt="" title="" />
                            <br/>
                            <span>Views</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" href="<?php echo make_load_url('about') ?>"><img src="assets/images/icons/white/about.png" alt="" title="" />
                            <br/>
                            <span>About</span>
                        </a>
                    </div>
                </div>
                <div class="col-33">
                    <div class="menu_content">
                        <a style="color: #fff;" class="item-link" onclick="location.href = '<?php echo make_admin_url('logout'); ?>'" style="cursor: pointer"><img src="assets/images/icons/white/lock.png" alt="" title="" />
                            <br/>
                            <span>Logout</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>