<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Add View <a id="add_buyer_link" style="display: none" data-buyer_sale_head_id="" href="<?php echo make_load_url('view', 'view', 'view') ?>" class="flot-right open-login-screen" alt="Add Buyer" title="Add Buyer">Add Buyer</a></h2>
        <?php
        if (!$sale_heads) {
            ?>
            <div class="alert alert-danger">
                No Sale Name Added!
            </div>
        <?php } else { ?>
            <div id="sale_head_html_details">
                <div class="contactform">
                    <label>Select Sale Name</label>
                    <br/>
                    <select name="sale_head_id" class="form_select active-state" id="sale_head_id_details">
                        <?php foreach ($sale_heads as $sale_head) { ?>
                            <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                        <?php } ?>
                    </select>
                    <br/>
                    <input type="submit" name="submit" class="button button-big button-fill" id="lot_buyer_submit" value="GO" />
                    <div>
                        <a class="button button-big button-fill back link" href="<?php echo make_load_url('home') ?>">CANCEL</a>
                    </div>
                </div>
            </div>

            <div id="lots_buyers_data"></div>
        <?php } ?>
    </div>
</div>
