<div class="page-content">
    <div class="content-block">
        <h2 class="page_title"><font color="#3b5998"><?php echo $view_count; ?></font> Views <a href="<?php echo make_load_url('view') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <form data-search-list=".list-block-search" data-search-in=".item-title" class="searchbar searchbar-init">
            <div class="searchbar-input">
                <input type="search" placeholder="Search"><a href="#" class="searchbar-clear"></a>
            </div><a href="#" class="searchbar-cancel">Cancel</a>
        </form>
        <div class="content-block searchbar-not-found">
            <div class="content-block-inner">No Buyer Found..!</div>
        </div>
        <div class="list-block list-block-search searchbar-found contacts-block" style="color: #000;">

            <div class="list-block accordion-list">
                <ul>
                    <?php foreach ($buyers as $k => $buyer) { ?>
                        <li style="margin: 0px !important" class="accordion-item">
                            <a href="#" class="item-link item-content">
                                <div class="item-inner">
                                    <div class="item-title">
                                        <?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?>
                                    </div>
                                </div>
                            </a>
                            <div class="accordion-item-content">
                                <div class="list-block">
                                    <ul>
                                        <?php
                                        $obj = new view;
                                        $views = $obj->getViewsByBuyer($buyer->id, $horse_id);
                                        foreach ($views as $view) {
                                            ?>
                                            <li id="li_<?php echo $view->id; ?>">
                                                <div class="item-content">
                                                    <div class="item-inner">
                                                        <div class="item-title" style="width: 100%">
                                                            <h4>
                                                                <a id="delete_view_detail_id" data-view_id="<?php echo $view->id; ?>" style="float: right" href="" alt="Delete View" title="Delete View"><i class="fa fa-times-circle fa-2x"></i></a>
                                                                    <?php echo $view->date_viewed; ?>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>