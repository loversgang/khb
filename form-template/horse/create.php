<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Add Horse <a href="<?php echo make_load_url('horse') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('horse') ?>">
                <label>Sale Name</label>
                <select name="sale_head_id" class="form_select active-state form_input">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
                <input type="text" name="name" value="" placeholder="Horse Name" class="form_input" />
                <input type="text" name="lot_number" value="" placeholder="Lot Number" class="form_input" />
                <input type="text" name="sire" value="" placeholder="Sire" class="form_input" />
                <input type="text" name="dam" value="" placeholder="Dam" class="form_input" />
                <input type="number" name="age" value="" placeholder="Age" class="form_input" />
                <label>Sex</label>
                <select name="sex" class="form_select active-state form_input">
                    <option value="Colt">Colt</option>
                    <option value="Filly">Filly</option>
                </select>
                <label>Status</label>
                <select name="status" class="form_select active-state form_input">
                    <option value="sold">Sold</option>
                    <option value="un_sold" selected>Not Sold</option>
                </select>
                <input type="submit" name="save_lot" class="button button-big button-fill" id="submit_lot_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('horse') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>