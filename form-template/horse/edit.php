<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Edit Horse <a href="<?php echo make_load_url('horse') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('horse') ?>">
                <label>Sale Name</label>
                <select name="sale_head_id" class="form_select active-state form_input">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>" <?php echo $horse->sale_head_id == $sale_head->id ? 'selected' : ''; ?>><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
                <input type="text" name="name" value="<?php echo $horse->name ?>" placeholder="Horse Name" class="form_input" />
                <input type="text" name="lot_number" value="<?php echo $horse->lot_number ?>" placeholder="Lot Number" class="form_input" />
                <input type="text" name="sire" value="<?php echo $horse->sire ?>" placeholder="Sire" class="form_input" />
                <input type="text" name="dam" value="<?php echo $horse->dam ?>" placeholder="Dam" class="form_input" />
                <input type="text" name="age" value="<?php echo $horse->age ?>" placeholder="Age" class="form_input" />
                <label>Sex</label>
                <select name="sex" class="form_select active-state form_input">
                    <option value="Colt" <?php echo $horse->sex == "Colt" ? 'selected' : ''; ?>>Colt</option>
                    <option value="Filly"<?php echo $horse->sex == "Filly" ? 'selected' : ''; ?>>Filly</option>
                </select>
                <label>Status</label>
                <select name="status" class="form_select active-state form_input">
                    <option value="sold" <?php echo $horse->status == 'sold' ? 'selected' : '' ?>>Sold</option>
                    <option value="un_sold" <?php echo $horse->status == 'un_sold' ? 'selected' : '' ?>>Not Sold</option>
                </select>
                <input type="hidden" name="id" value="<?php echo $horse->id ?>"/>
                <input type="submit" name="update_lot" class="button button-big button-fill" id="update_lot_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('horse') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>