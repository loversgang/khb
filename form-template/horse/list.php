<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Horses / Lots <a href="<?php echo make_load_url('horse', 'insert', 'insert') ?>" class="flot-right link" alt="Add Lot" title="Add Lot"><i class="fa fa-plus-circle fa-2x"></i></a></h2>
        <div id="show_notification_message"></div>
        <?php
        if (!$sale_heads) {
            ?>
            <div class="alert alert-danger">
                No Sale Name Added!<br/>
                <a href="<?php echo make_load_url('sale_head', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Sale Names.
            </div>
        <?php } else { ?>
            <div class="contactform">
                <label>Select Sale Name</label>
                <select name="sale_head_id" class="form_select active-state" id="sale_head_cat">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
                <label>Active Horses</label>
            </div>
            <div id="horse_data"></div>
        <?php } ?>
    </div>
</div>