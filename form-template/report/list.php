<div class="page-content">
    <div class="content-block">
        <h2 class="page_title" style="text-align: center">Reports <a style="display: none;cursor: pointer" id="back_to_reports" class="flot-right" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div id="reports_data"></div>
        <ul id="all_reports_list">
            <li>
                <div class="feat_small_details">
                    <a href="" class="button button-big button-fill link" id="all_lots_total_views_report" style="cursor: pointer">
                        <h4>ALL LOTS TOTAL VIEWS</h4>
                    </a>
                </div>
            </li>
            <li>
                <div class="feat_small_details">
                    <a href="" class="button button-big button-fill link" id="lots_buyers_report" style="cursor: pointer">
                        <h4>ALL LOTS BY BUYERS</h4>
                    </a>
                </div>
            </li>
            <li>
                <div class="feat_small_details">
                    <a href="" class="button button-big button-fill link" id="lots_report" style="cursor: pointer">
                        <h4>ALL VIEWS BY HORSES</h4>
                    </a>
                </div>
            </li>
            <li>
                <div class="feat_small_details">
                    <a href="" class="button button-big button-fill link" id="all_lots_report" style="cursor: pointer">
                        <h4>RESULTS BY ALL LOTS</h4>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>