<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Buyers <a href="<?php echo make_load_url('buyer', 'insert', 'insert') ?>" class="flot-right link" alt="Add Buyer" title="Add Buyer">Add Buyer<br/><center><i class="fa fa-plus-circle fa-2x"></i></center></a>
            <br/><a style="color: #3b5998" href="<?php echo make_load_url('view') ?>" class="link" alt="Back" title="Back">Back To Views</a></h2>
        <div id="show_notification_message"></div>
        <?php if (!$buyers) { ?>
            <div class="alert alert-danger">
                No Buyer Added!<br/>
                <a href="<?php echo make_load_url('buyer', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Buyers.
            </div>
        <?php } else { ?>
            <form data-search-list=".list-block-search" data-search-in=".item-title" class="searchbar searchbar-init">
                <div class="searchbar-input">
                    <input type="search" placeholder="Search"><a href="#" class="searchbar-clear"></a>
                </div><a href="#" class="searchbar-cancel">Cancel</a>
            </form>
            <div class="content-block searchbar-not-found">
                <div class="content-block-inner">No Buyer Found..!</div>
            </div>
            <div class="list-block list-block-search searchbar-found contacts-block">
                <?php
                $buyers_assoc = array();
                foreach ($buyers as $k => $buyer) {
                    $first_char = ucfirst(substr($buyer->first_name, 0, 1));
                    $buyers_assoc[$first_char][] = $buyer;
                }
                foreach ($buyers_assoc as $first_char => $buyer_list) {
                    ?>
                    <div class="list-group">
                        <ul>
                            <li class="list-group-title"><?php echo $first_char; ?></li>
                            <?php foreach ($buyer_list as $buyer) { ?>
                                <li>
                                    <div class="item-content">
                                        <div class="item-inner">
                                            <div class="item-title">
                                                <a class="buyers_list item-link" href="<?php echo make_load_url('buyer', 'update', 'update', 'id=' . $buyer->id) ?>"><h4><?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?></h4>
                                                    <?php echo ucfirst($buyer->city); ?> <?php echo ucfirst($buyer->country); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>