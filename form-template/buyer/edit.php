<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Edit Buyer <a href="<?php echo make_load_url('buyer') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <?php if ($_SERVER['HTTP_REFERER'] == DIR_WS_SITE . 'user.php') { ?>
                <form method="post" action="<?php echo 'user.php#!/' . make_load_url('buyer') ?>">
                <?php } else { ?>
                    <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('buyer') ?>">
                    <?php } ?>
                    <input type="text" name="first_name" value="<?php echo $buyer->first_name; ?>" placeholder="First Name" class="form_input" />
                    <input type="text" name="last_name" value="<?php echo $buyer->last_name; ?>" placeholder="Last Name" class="form_input" />
                    <input type="text" name="email" data-skip="true" value="<?php echo $buyer->email; ?>" placeholder="Email" class="form_input" />
                    <input type="number" name="phone" data-skip="true" value="<?php echo $buyer->phone; ?>" placeholder="Phone" class="form_input" />
                    <input type="text" name="city" data-skip="true" value="<?php echo $buyer->city; ?>" placeholder="City" class="form_input" />
                    <input type="text" name="state" data-skip="true" value="<?php echo $buyer->state; ?>" placeholder="State" class="form_input" />
                    <input type="text" name="country" data-skip="true" value="<?php echo $buyer->country; ?>" placeholder="Country" class="form_input" />
                    <input type="text" name="remarks" data-skip="true" placeholder="Remarks" value="<?php echo $buyer->remarks; ?>" class="form_input" />
                    <input type="hidden" name="id" value="<?php echo $buyer->id; ?>" class="form_input" />
                    <input type="submit" name="update_buyer" class="button button-big button-fill" id="update_buyer_btn" value="SAVE" />
                </form>
                <div>
                    <a class="button button-big button-fill back link" href="<?php echo make_load_url('buyer') ?>">CANCEL</a>
                </div>
        </div>
    </div>
</div>