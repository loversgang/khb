<div class="page-content">
    <div class="content-block">
        <h2 class="page_title" style="text-align: center">About</h2>
        <div class="alert alert-info">
            <font style="font-size: 22px;word-wrap: break-word">
            KHB Sales Web App is private web app to be used by Keith Harte Bloodstock Limited, United Kingdom.
            </font>
        </div>
    </div>
</div>