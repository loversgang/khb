<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Users <a href="<?php echo make_load_url('user', 'insert', 'insert') ?>"class="flot-right link" alt="Add User" title="Add User"><i class="fa fa-plus-circle fa-2x"></i></a></h2>
        <div id="show_notification_message"></div>
        <?php if (!$users) { ?>
            <div class="alert alert-danger">
                No User Added!<br/>
                <a href="<?php echo make_load_url('user', 'insert', 'insert') ?>" style="color: #3b5998"><b>Click Here</b></a>  To Add Users.
            </div>
        <?php } else { ?>
            <ul class="features_list_detailed">
                <?php foreach ($users as $user) { ?>
                    <li>
                        <div class="feat_small_details">
                            <a href="<?php echo make_load_url('user', 'update', 'update', 'id=' . $user->id) ?>"> <h4><?php echo ucfirst($user->first_name) ?> <?php echo ucfirst($user->last_name) ?> </h4>
                                <font style="font-size: 15px"><?php echo $user->phone ?><br/>
                                <?php echo $user->email ?></font>
                            </a>
                        </div>        
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
</div>