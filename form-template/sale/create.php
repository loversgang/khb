<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Add Result <a href="<?php echo make_load_url('sale') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('sale') ?>">
                <label>Sale Name</label>
                <select name="sale_head_id" class="form_select active-state form_input" id="sale_head_cat_result">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>"><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
                <div id="lots_data"></div>
                <label>Buyer</label>               
                <select name="buyer_id" class="form_select form_input">
                    <?php foreach ($buyers as $buyer) { ?>
                        <option value="<?php echo $buyer->id; ?>"><?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?></option>
                    <?php } ?>
                </select>
                <input type="number" name="amount" value="" placeholder="Sales Amount(&#163;)" class="form_input" />
                <input type="text" name="date_add" class="form_input" placeholder="Sale Date" id="calendar-default">
                <input type="submit" name="save_result" class="button button-big button-fill" id="submit_result_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('sale') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>