<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Add Result <a href="<?php echo make_load_url('sale') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('sale') ?>">
                <label>Sale Name</label>
                <select name="sale_head_id" class="form_select active-state form_input" id="sale_head_cat_result">
                    <?php foreach ($sale_heads as $sale_head) { ?>
                        <option value="<?php echo $sale_head->id; ?>" <?php echo $sale->sale_head_id == $sale_head->id ? 'selected' : ''; ?>><?php echo $sale_head->title; ?></option>
                    <?php } ?>
                </select>
                <div id="lots_data"></div>
                <label>Buyer</label>               
                <select name="buyer_id" class="form_select form_input">
                    <?php foreach ($buyers as $buyer) { ?>
                        <option value="<?php echo $buyer->id; ?>" <?php echo $sale->buyer_id == $buyer->id ? 'selected' : ''; ?>><?php echo ucfirst($buyer->first_name); ?> <?php echo ucfirst($buyer->last_name); ?></option>
                    <?php } ?>
                </select>
                <input type="number" name="amount" value="<?php echo $sale->amount ?>" placeholder="Sales Amount(&#163;)" class="form_input" />
                <input readonly type="text" name="date_add" value="<?php echo $sale->date_add ?>" placeholder="Sale Date" id="calendar-default" class="form_input">
                <input type="hidden" name="id" value="<?php echo $sale->id ?>" class="form_input">
                <input type="submit" name="update_result" class="button button-big button-fill" id="update_result_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('sale') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>