<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Sale Name <a href="<?php echo make_load_url('sale_head') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('sale_head') ?>">
                <input type="text" name="title" value="" placeholder="Sale Name" class="form_input" />
                <table>
                    <td>
                        <label>Active:</label>
                    </td>
                    <td>
                        <div style="margin: 10px;  width: 100%; display: inline-block">
                            <input type="radio" id="radio1" name="is_active" value="1" checked="">
                            <label style="display: inline" for="radio1"><i class="fa fa-check"></i></label>
                            <input type="radio" id="radio2" name="is_active" value="0">
                            <label style="display: inline" for="radio2"><i class="fa fa-times"></i></label>
                        </div>
                    </td>
                </table>
                <input type="submit" name="save_sale_head" class="button button-big button-fill" id="submit_sale_head_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('sale_head') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>