<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Sale Name <a href="<?php echo make_load_url('sale_head', 'insert', 'insert') ?>" class="flot-right link" alt="Add Sale Name" title="Add Sale Name"><i class="fa fa-plus-circle fa-2x"></i></a></h2>
        <div id="show_notification_message"></div>
        <ul class="features_list_detailed">
            <?php foreach ($sale_heads as $sale_head) { ?>
                <li>
                    <div class="feat_small_details">                                
                        <a href="<?php echo make_load_url('sale_head', 'update', 'update', 'id=' . $sale_head->id) ?>">
                            <h4><?php echo $sale_head->title; ?>
                                <?php if ($sale_head->is_active == '1') { ?>
                                    <img  src="assets/images/icons/black/green_button.png"/>
                                <?php } else { ?>
                                    <img src="assets/images/icons/black/grey_button.png"/>
                                <?php } ?>
                            </h4>
                        </a>
                    </div>            
                </li>
            <?php } ?>
        </ul>
    </div>
</div>