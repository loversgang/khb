<?php

include_once(DIR_FS_SITE . 'include/functionClass/saleClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
if ($_SESSION['admin_session_secure']['user_type'] == 'user') {
    Redirect(DIR_WS_SITE . 'user.php');
}
$modName = 'sale_head';
#handle actions here.
switch ($action):
    case'list':
        $obj = new sale_head;
        $sale_heads = $obj->listAllSaleHeads();
        break;
    case'insert':
        break;
    case'update':
        $obj = new sale_head;
        $sale_head = $obj->getSaleHead($id);
        break;
    case'delete':
        break;
    default:break;
endswitch;
