<?php

include_once(DIR_FS_SITE . 'include/functionClass/saleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/buyerClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
if ($_SESSION['admin_session_secure']['user_type'] == 'user') {
    Redirect(DIR_WS_SITE . 'user.php');
}

$modName = 'sale';
#handle actions here.
switch ($action):
    case'list':
        // List Sale Heads
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        break;

    case'insert':
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();

        $obj = new buyer;
        $buyers = $obj->listBuyers();
        break;
    case'update':
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();

        $obj = new buyer;
        $buyers = $obj->listBuyers();

        $obj = new sale;
        $sale = $obj->getSale($id);
        break;
    case'delete':
        break;
    default:break;
endswitch;
?>
