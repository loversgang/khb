<?php

include_once(DIR_FS_SITE . 'include/functionClass/buyerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/saleClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$modName = 'buyer';

#handle actions here.
switch ($action):
    case'list':
        $obj = new buyer;
        $buyers = $obj->listBuyers();
        break;
    case'insert':
        // List Sale Heads
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        break;
    case'update':
        // List Sale Heads
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        
        // Buyer Detail
        $obj = new buyer;
        $buyer = $obj->getBuyer($id);
        break;
    case'delete':
        break;
    default:break;
endswitch;
