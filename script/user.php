<?php

include_once(DIR_FS_SITE . 'include/functionClass/adminUserClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
if ($_SESSION['admin_session_secure']['user_type'] == 'user') {
    Redirect(DIR_WS_SITE . 'user.php');
}
$modName = 'user';
#handle actions here.
switch ($action):
    case'list':
        $obj = new admin;
        $users = $obj->listAdminUsers();
        break;
    case'insert':
        break;
    case'update':
        // Get Admin User
        $obj = new admin;
        $user = $obj->getAdminUser($id);
        break;
    case'delete':
        break;
    default:break;
endswitch;
?>
