<?php

function include_functions($functions) {
    foreach ($functions as $value):
        if (file_exists(DIR_FS_SITE . 'include/function/' . $value . '.php')):
            include_once(DIR_FS_SITE . 'include/function/' . $value . '.php');
        endif;
    endforeach;
}

function __autoload($class_name) {
    include DIR_FS_SITE . 'include/functionClass/' . $class_name . '.php';
}

function display_message($unset = 0) {
    $admin_user = new admin_session();

    if ($admin_user->isset_pass_msg()):
        if (isset($_SESSION['admin_session_secure']['msg_type']) && $_SESSION['admin_session_secure']['msg_type'] == '1'):
            $error_type = 'danger';
        else:
            $error_type = 'success';
        endif;
        foreach ($admin_user->get_pass_msg() as $value):
            echo '<div class="fadeOut-notif alert alert-' . $error_type . '"><strong>';
            echo $value;
            echo '</strong></div>';
        endforeach;
    endif;
    ($unset) ? $admin_user->unset_pass_msg() : '';
}

function get_var_if_set($array, $var, $preset = '') {

    return isset($array[$var]) ? $array[$var] : $preset;
}

function get_var_set($array, $var, $var1) {

    if (isset($array[$var])):

        return $array[$var];

    else:

        return $var1;

    endif;
}

function get_template($template_name, $array, $selected = '') {

    include_once(DIR_FS_SITE . 'template/' . TEMPLATE . '/' . $template_name . '.php');
}

function get_meta($page) {

    $page = trim($page);

    if ($page != ''):

        $query = new query('keywords');

        $query->Where = "where page_name='$page'";

        if ($content = $query->DisplayOne()):

            return $content;

        else:

            return null;

        endif;

    endif;

    return null;
}

function head($content = '') {

    /* include all the header related things here... like... common meta tags/javascript files etc. */

    global $page;

    global $content;



    if (is_object($content)):
        ?>

        <title><?php echo isset($content->name) && $content->name ? $content->name : ''; ?></title>	


        <meta name="viewport" content="width=device-width,user-scale=1"/>

        <meta name="keywords" content="<?php echo isset($content->meta_keyword) ? $content->meta_keyword : ''; ?>" />

        <meta name="description" content="<?php echo isset($content->meta_description) ? $content->meta_description : ''; ?>" />

        <?php
    else:

        $content = get_meta($page);
        ?>

        <title><?php echo isset($content->page_title) && $content->page_title ? $content->page_title : ''; ?></title>	

        <meta name="keywords" content="<?php echo isset($content->keyword) ? $content->keyword : ''; ?>" />

        <meta name="description" content="<?php echo isset($content->description) ? $content->description : ''; ?>" />

    <?php endif; ?>



    <link rel="shortcut icon" href="<?php echo DIR_WS_SITE_GRAPHIC ?>favicon.ico" />

    <?php include_once(DIR_FS_SITE . 'include/template/stats/google_analytics.php'); ?>

    <?php
}

function css($array = array('reset', 'master')) {

    echo '<link href="' . DIR_WS_SITE . 'css/print.css" rel="stylesheet" type="text/css" media="print" >' . "\n";

    echo '<link href="' . DIR_WS_SITE . 'css/base.css" rel="stylesheet" type="text/css" media="screen" >' . "\n";



    foreach ($array as $k => $v):

        if ($v == 'style' && isset($_SESSION['use_stylesheet'])):

            echo '<link href="' . DIR_WS_SITE . 'css/' . $_SESSION['use_stylesheet'] . '.css" rel="stylesheet" type="text/css" media="screen, projection" >' . "\n";

        else:

            echo '<link href="' . DIR_WS_SITE . 'css/' . $v . '.css" rel="stylesheet" type="text/css" media="screen, projection" >' . "\n";

        endif;

    endforeach;

    echo '<link href="' . DIR_WS_SITE . 'javascript/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" media="screen" >' . "\n";

    echo '<link href="' . DIR_WS_SITE . 'css/validation/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" media="screen" >' . "\n";
}

function js($array = array('jquery-1.2.6.min', 'search-reset')) {

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation/js/jquery-1.7.2.min.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'fancybox/lib/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/fancybox/jquery.mousewheel-3.0.4.pack.js" type="text/javascript"></script> ' . "\n";





    foreach ($array as $k => $v):

        echo '<script src="' . DIR_WS_SITE . 'javascript/' . $v . '.js" type="text/javascript"></script> ' . "\n";

    endforeach;

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation/js/languages/jquery.validationEngine-en.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation/js/jquery.validationEngine.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/email/resettable.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/fancybox.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/sharethis.js" type="text/javascript"></script> ' . "\n";
}

function body() {

    /* # include all the body related things like... tracking code here. */
}

function footer() {

    /* # if you need to add something to the website footer... please add here. */
}

function array_map_recursive($callback, $array) {

    $b = Array();

    foreach ($array as $key => $value) {

        $b[$key] = is_array($value) ? array_map_recursive($callback, $value) : call_user_func($callback, $value);
    }

    return $b;
}

function if_set_in_post_then_display($var) {

    if (isset($_POST[$var])):

        echo $_POST[$var];

    endif;

    echo '';
}

function set_data_in_session($POST) {

    unset($_SESSION['user_session']['register_data']);
    $register_data = array();
    $register_data['first_name'] = isset($POST['first_name']) ? $POST['first_name'] : "";
    $register_data['last_name'] = isset($POST['first_name']) ? $POST['last_name'] : "";
    $register_data['username'] = isset($POST['first_name']) ? $POST['username'] : "";
    $_SESSION['user_session']['register_data'] = $register_data;
}

function if_set_in_session_then_display($var) {

    if (isset($_SESSION['user_session']['register_data'][$var])):

        echo $_SESSION['user_session']['register_data'][$var];

    endif;

    echo '';
}

function validate_captcha() {

    global $privatekey;



    if ($_POST["recaptcha_response_field"]) {

        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if ($resp->is_valid) {

            return true;
        } else {

            /* # set the error code so that we can display it */

            return false;
        }
    }
}

function is_set($array = array(), $item, $default = 1) {

    if (isset($array[$item]) && $array[$item] != 0) {

        return $array[$item];
    } else {

        return $default;
    }
}

function limit_text($text, $limit = 100) {

    if (strlen($text) > $limit):

        return substr($text, 0, strpos($text, ' ', $limit));

    else:

        return $text;

    endif;
}

function get_object($tablename, $id, $type = 'object') {

    $query = new query($tablename);

    $query->Where = "where id='$id'";

    return $query->DisplayOne($type);
}

function get_object_by_col($tablename, $col, $col_value, $type = 'object') {

    $query = new query($tablename);

    $query->Where = "where $col='$col_value' AND is_deleted='0'";

    return $query->DisplayOne($type);
}

function get_object_by_col_without_delete($tablename, $col, $col_value, $type = 'object') {

    $query = new query($tablename);

    $query->Where = "where $col='$col_value'";

    return $query->DisplayOne($type);
}

function get_object_var($tablename, $id, $var) {

    $q = new query($tablename);

    $q->Field = "$var";

    $q->Where = "where id='" . $id . "'";

    if ($obj = $q->DisplayOne()):

        return $obj->$var;

    else:

        return false;

    endif;
}

function echo_y_or_n($status) {

    echo ($status) ? 'Yes' : 'No';
}

function target_dropdown($name, $selected = '', $tabindex = 1) {

    $values = array('new window' => '_blank', 'same window' => '_parent');

    echo '<select name="' . $name . '" size="1" tabindex="' . $tabindex . '">';

    foreach ($values as $k => $v):

        if ($v == $selected):

            echo '<option value="' . $v . '" selected>' . ucfirst($k) . '</option>';

        else:

            echo '<option value="' . $v . '">' . ucfirst($k) . '</option>';

        endif;

    endforeach;

    echo '</select>';
}

function make_csv_from_array($array) {

    $sr = 1;

    $heading = '';

    $file = '';

    foreach ($array as $k => $v):

        foreach ($v as $key => $value):

            if ($sr == 1):$heading.=$key . ', ';
            endif;

            $file.=str_replace("\r\n", "<<>>", str_replace(",", ".", html_entity_decode($value))) . ', ';

        endforeach;

        $file = substr($file, 0, strlen($file) - 2);

        $file.="\n";

        $sr++;

    endforeach;

    return $file = $heading . "\n" . $file;
}

function get_y_n_drop_down($name, $selected) {

    echo '<select class="regular" name="' . $name . '">';

    if ($selected):

        echo '<option value="1" selected>Yes</option>';

        echo '<option value="0">No</option>';

    else:

        echo '<option value="0" selected>No</option>';

        echo '<option value="1">Yes</option>';

    endif;

    echo '</select>';
}

function get_setting_control($key, $type, $value) {

    switch ($type) {

        case 'text':

            echo '<input type="text" name="key[' . $key . ']" value="' . $value . '" class="form-control">';

            break;

        case 'textarea':

            echo '<textarea class="form-control" name="key[' . $key . ']" rows="3" tabindex="6">' . $value . '</textarea>';

            break;

        case 'select':

            echo get_y_n_drop_down('key[' . $key . ']', $value);

            break;

        default: echo get_y_n_drop_down('key[' . $key . ']', $value);
    }
}

function css_active($page, $value, $class) {

    if ($page == $value) echo 'class=' . $class;
}

function parse_into_array($string) {

    return explode(',', $string);
}

function get_section_heading($Page) {

    switch ($Page) {

        case 'product':

        case 'category':

        case 'cat_product':

        case 'product_search':

        case 'product_map':

        case 'product_collection':

        case 'product_check':

            return 'Product Manager';

            break;

        case 'review':

            return 'Review Manager';

            break;

        case 'gallery':

            return 'Gallery Manager';

            break;

        case 'gallery_image':

            return 'Image Manager';

            break;

        case 'meet_the_team':

            return 'Team Manager';

            break;

        case 'project':

            return 'Project Manager';

            break;

        case 'project_image':

            return 'Project Image Manager';

            break;

        case 'user':

        case 'search_user':

        case 'udetail':

            return 'User Manager';

            break;

        case 'jobposting':

            return 'Job Postings Manager';

            break;

        case 'news':

            return 'News Manager';

            break;

        case 'link_heading':

            return 'Links Heading Manager';

            break;

        case 'links':

            return 'Links Manager';

            break;

        case 'department':

            return 'Department Manager';

            break;

        case 'business_category':

            return 'Business Directory Manager';

            break;

        case 'directory':

            return 'Business Directory Manager';

            break;

        case 'quick_pos':

            return 'Home Manager';

            break;

            break;

        case 'quick_pos_image':

            return 'Manage Home Images';

            break;

        case 'discount':

            return 'Discounts Manager';

            break;

        case 'setting':

            return 'Website Settings';

            break;

        case 'content':return 'Page Manager';

            break;

        case 'content_collection':

        case 'content_navigation':return 'Navigation Manager';

            break;

        case 'content_photo':

            return 'Content Manager';

            break;

        case 'order':

        case 'order_d':

        case 'a_order':

        case 'archive':

        case 'search_order':

            return 'Order Manager';

            break;

        case 'all_attribute':

            return 'Attribute Manager';

            break;

        case 'email':

            return 'Email Manager';

            break;

        case 'new_letter':

        case 'letters':

        case 'send_to':

            return 'Newsletter Manager';

            break;

        case 'event':

            return 'Event Manager';

            break;

        case 'videos':

            return 'Videos Manager';

            break;

        case 'faq':

            return 'FAQ Manager';

            break;

        case 'articles':

            return 'Article Manager';

            break;

        case 'blog':

            return 'Blog Manager';

            break;

        case 'success_stories':

            return 'Success Story Manager';

            break;



        case 'keywords_management':

            return 'Keywords Management';

            break;



        case 'services':

            return 'Footer Service Links Manager';

            break;

        case 'cities':

            return 'Footer City Links Manager';

            break;

        case 'glossary':

            return 'Glossary Manager';

            break;

        case 'admin':

            return 'Admin Manager';

            break;



        case 'banner':

            return 'Slider Manager';

            break;

        case 'zones':

        case 'ship':

        case 'zone_country':

        case 'country':

            return 'Delivery Manager';

            break;

        case 'home':

            return 'Dashboard';

            break;

        case 'upload_logo':

            return 'Company Links Manager';

            break;

        default:

            return $Page;
    }
}

function MakeDataArray($post, $not) {

    $data = array();

    foreach ($post as $key => $value):

        if (!in_array($key, $not)):

            $data[$key] = $value;

        endif;

    endforeach;

    return $data;
}

function is_var_set_in_post($var, $check_value = false) {

    if (isset($_POST[$var])):

        if ($check_value):

            if ($_POST[$var] === $check_value):

                return true;

            else:

                return false;

            endif;

        endif;

        return true;

    else:

        return false;

    endif;
}

function display_form_error() {

    $login_session = new user_session();

    if ($login_session->isset_pass_msg()):

        $array = $login_session->get_pass_msg();
        ?>

        <div class="errorMsg">

            <?php
            foreach ($array as $value):

                echo $value . '<br/>';

            endforeach;
            ?>

        </div>

    <?php elseif (isset($_GET['msg']) && $_GET['msg'] != ''):
        ?>

        <div class="errorMsg">

            <?php echo urldecode($_GET['msg']) . '<br/>'; ?>

        </div>

        <?php
    endif;

    $login_session->isset_pass_msg() ? $login_session->unset_pass_msg() : '';
}

function Redirect($URL) {
    @header("location:$URL", TRUE);
    exit;
}

function Redirect1($filename) {

    if (!headers_sent()) header('Location: ' . $filename);

    else {

        echo '<script type="text/javascript">';

        echo 'window.location.href="' . $filename . '";';

        echo '</script>';

        echo '<noscript>';

        echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';

        echo '</noscript>';
    }
}

function re_direct($URL) {

    header("location:$URL");

    exit;
}

/* function make_url($page, $query=null)

  {

  return DIR_WS_SITE.'?page='.$page.'&'.$query;

  } */

function display_url($title, $page, $query = '', $class = '') {

    return '<a href="' . make_url($page, $query) . '" class="' . $class . '">' . $title . '</a>';
}

function make_admin_url($page, $action = 'list', $section = 'list', $query = '') {

    return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . ($query ? '&' . $query : '');
}

function make_load_url($page, $action = 'list', $section = 'list', $query = '') {

    return 'control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . ($query ? '&' . $query : '');
}

function load_action_url($page, $action = 'list') {

    return 'control.php?Page=' . $page . '&action=' . $action;
}

function make_admin_url2($page, $action = 'list', $section = 'list', $query = '') {

    if ($page == 'home'):

        return DIR_WS_SITE . 'index.php';

    else:

        return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action2=' . $action . '&section2=' . $section . '&' . $query;

    endif;
}

function prepare_query($queryString) {



    $string = '';

    parse_str($queryString, $string);

    switch ($string['page']):

        case 'content':

            $query = new query('content');

            $name = $string['id'];

            $query->Where = "where name='$name'";

            $object = $query->DisplayOne();

            $_GET['id'] = $object->id;

            $_REQUEST['id'] = $object->id;

            break;

        case 'category':

            $category_name = strtolower(str_replace('-', " ", $string['id']));

            $id = get_category_id_by_name($category_name);

            $_GET['id'] = $id;
            $_REQUEST['id'] = $id;

            isset($string['p']) ? $_GET['p'] = $string['p'] : '';

            $_GET['page'] = 'product';
            $_REQUEST['page'] = 'product';

            break;

        case 'pdetail':

            $category_name = strtolower(str_replace('-', " ", $string['id']));

            if ($id = get_product_id_by_url_name($category_name)):

                $_GET['id'] = $id;
                $_REQUEST['id'] = $id;

                $_GET['page'] = 'pdetail';
                $_REQUEST['page'] = 'pdetail';

            elseif ($id = get_product_id_by_product_name($category_name)):

                $_GET['id'] = $id;
                $_REQUEST['id'] = $id;

                $_GET['page'] = 'pdetail';
                $_REQUEST['page'] = 'pdetail';

            endif;

            break;

        case 'wish_list':

            $_GET['page'] = 'wish_list';
            $_REQUEST['page'] = 'wish_list';

            isset($string['id']) ? $_GET['id'] = $string['id'] : '';

            isset($string['id']) ? $_REQUEST['id'] = $string['id'] : '';

            isset($string['delete']) ? $_GET['delete'] = 1 : '';

            isset($string['add_wishlist']) ? $_GET['add_wishlist'] = 1 : '';

            break;

        case 'search':

            $_GET['page'] = 'search';
            $_REQUEST['page'] = 'search';

            isset($string['category']) ? $_GET['category'] = $string['category'] : '';

            isset($string['keyword']) ? $_GET['keyword'] = $string['keyword'] : '';

            isset($string['p']) ? $_GET['p'] = $string['p'] : '';

            break;

        case 'payment':

            isset($string['id']) ? $_GET['id'] = $string['id'] : '';

            break;

        case 'home':

            isset($string['download']) ? $_GET['download'] = $string['download'] : '';

            break;

        default:

            isset($string['id']) ? $_GET['id'] = $string['id'] : '';

            isset($string['msg']) ? $_GET['msg'] = str_replace('-', ' ', $string['msg']) : '';

            !isset($_GET['page']) ? $_GET['page'] = 'home' : '';

            break;

    endswitch;
}

function make_admin_link($url, $text, $title = '', $class = '', $alt = '') {

    return '<a href="' . $url . '" class="' . $class . '" title="' . $title . '" alt="' . $alt . '" >' . $text . '</a>';
}

function quit($message = 'processing stopped here') {

    echo $message;

    exit;
}

function download_orders($payment_status, $order_status) {

    $orders = new query('orders');

    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";

    if ($order_status == 'paid'):

        $orders->Where = "where payment_status=" . $payment_status . " and order_status!='delivered'";

    elseif ($order_status == 'attempted'):

        $orders->Where = "where payment_status=" . $payment_status . " and order_status='received'";

    else:

        $orders->Where = "where payment_status=" . $payment_status . " and order_status='delivered'";

    endif;

    $orders->DisplayAll();



    $orders_arr = array();

    if ($orders->GetNumRows()):

        while ($order = $orders->GetArrayFromRecord()):

            $order['Username'] = get_username_by_orders($order['user_id']);

            array_push($orders_arr, $order);

        endwhile;

    endif;

    $file = make_csv_from_array($orders_arr);

    $filename = "orders" . '.csv';

    $fh = @fopen('download/' . $filename, "w");

    fwrite($fh, $file);

    fclose($fh);

    download_file('download/' . $filename);
}

function download_orders_by_criteria($from_date, $to_date, $payment_status, $order_status) {

    $orders = new query('orders');

    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";

    $orders->Where = "where order_status='$order_status' AND payment_status='$payment_status' AND (order_date BETWEEN CAST('$from_date' as DATETIME) AND CAST('$to_date' as DATETIME)) order by order_date";

    $orders->DisplayAll();



    $orders_arr = array();

    if ($orders->GetNumRows()):

        while ($order = $orders->GetArrayFromRecord()):

            $order['Username'] = get_username_by_orders($order['user_id']);

            array_push($orders_arr, $order);

        endwhile;

    endif;

    $file = make_csv_from_array($orders_arr);

    $filename = "orders" . '.csv';

    $fh = @fopen('download/' . $filename, "w");

    fwrite($fh, $file);

    fclose($fh);

    download_file('download/' . $filename);
}

function get_username_by_orders($id) {
    if ($id == 0):

        return 'Guest';

    elseif ($id):

        $q = new query('user');

        $q->Field = "firstname,lastname";

        $q->Where = "where id='" . $id . "'";

        $o = $q->DisplayOne();

        return $o->firstname;

    endif;
}

function get_zones_box($selected = 0) {

    $q = new query('zone');

    $q->DisplayAll();

    echo '<select name="zone" size="1">';

    while ($obj = $q->GetObjectFromRecord()):

        if ($selected = $obj->id):

            echo '<option value="' . $obj->id . '" selected>' . $obj->name . '</option>';

        else:

            echo '<option value="' . $obj->id . '">' . $obj->name . '</option>';

        endif;

    endwhile;

    echo '</select>';
}

function zone_drop_down($zone_id, $id, $s, $z) {

    $query = new query('zone_country');

    $query->Where = "where zone_id=$zone_id";

    $query->DisplayAll();

    $country_list = array();

    $country_name = '';

    while ($object = $query->GetObjectFromRecord()):

        $country_name = get_country_name_by_id($object->country_id);

        $idd = $object->country_id;

        /* $country_list('id'=>$object->id, 'name'=>$country_name)); */

        array_push($country_list, array('name' => $country_name, 'id' => $object->id));

    /* $country_list[$object->id]=$country_name; */

    endwhile;

    $total_list = array();

    foreach ($country_list as $k => $v):

        $total_list[] = $v['name'];

    endforeach;

    array_multisort($total_list, SORT_ASC, $country_list);



    echo '<select name="' . $id . '" size="10" style="width:200px;" multiple>';

    foreach ($country_list as $k => $v):

        if (($z == $zone_id) && $s):

            echo '<option value="' . $v['id'] . '" selected="selected">' . ucfirst($v['name']) . '</option>';

        else:

            echo '<option value="' . $v['id'] . '">' . ucfirst($v['name']) . '</option>';

        endif;

    endforeach;

    echo'</select>';
}

function get_page_head_description($page = 'home') {

    $defoult_text_for_home = "Whether you live in the province, have visited Newfoundland, or are a homesick Newfie looking for some much-needed reminders of The Rock, Island Treasures is your one source for everything Newfoundland and Labrador.";

    $defoult_text = "Mauris hendrerit ligula ut sapien varius in adipiscing nisl cursus. Nullam varius lacinia hendrerit. Ut eleifend porttitor porta. In et justo justo, eget dignissim lorem. Etiam eleifend enim eu purus fermentum a vulputate diam dapibus.";

    switch ($page) {

        case 'product':

            global $category;

            if (isset($_GET['id'])):

                echo html_entity_decode(limit_text($category->description, 303));

            else:

                echo html_entity_decode($defoult_text);

            endif;

            break;

        case 'content':

            global $object;

            echo html_entity_decode(limit_text($object->top_content, 303));

            break;

        case 'home':

            global $object;

            echo html_entity_decode($defoult_text_for_home);

            break;

        case 'pdetail':

            global $object;

            echo html_entity_decode(limit_text($object->top_description, 303));

            break;

        default:

            echo html_entity_decode($defoult_text);
    }
}

/* Function to add meta tags to content pages */

function add_metatags($title = "", $keyword = "", $description = "") {
    /* description rule */
    $description_length = 150; /* characters */
    if (strlen($description) > $description_length) $description = substr($description, 0, $description_length);

    /* title rule */
    $title_length = 100;
    if (strlen($title) > $title_length) $title = SITE_NAME . ' | ' . substr($title, 0, $title_length);
    else $title = SITE_NAME . ' | ' . $title;
    $content = new stdClass();
    $content->name = ucwords($title);
    $content->meta_keyword = $keyword;
    $content->meta_description = ucfirst($description);
    return $content;
}

/* sanitize funtion */

function sanitize($value) {

    $prohibited_chars = array(" ", "/", "$", "&", "'", '%', '"', "@");

    foreach ($prohibited_chars as $k => $v):

        $value = str_replace($v, '-', $value);

    endforeach;

    return strtolower($value);
}

/* For clarenville project */

function selected_nonselected_link($page, $navigation_link, $navigation_query) {

    global $id_mapping;

    $class_select_nav = "";

    if ($page == $navigation_link):

        if (isset($navigation_query) && !empty($navigation_query)):



            if ($navigation_query == $id_mapping[$page] . '=' . $_GET[$id_mapping[$page]]):

                $class_select_nav = "selected_nav_left";

            endif;

        else:



            $class_select_nav = "selected_nav_left";



        endif;



    endif;

    return $class_select_nav;
}

function get_object_from_table($tablename, $id, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where $id";
//$query->print=1;
    return $query->DisplayOne($type);
}

function get_user_email_variables($id) {
    $user_info = array();
    $query = new query('user');
    $query->Field = "id,username AS email,first_name,last_name,address1,phone,city,zip,state";
    $query->Where = 'where id=' . $id;
    $user = $query->DisplayOne('array');
    if (count($user)):
        foreach ($user as $k => $v):
            $user_info['USER_' . strtoupper($k)] = $v;
        endforeach;
        $user_info['FIRST_NAME'] = $user['first_name'];
        $user_info['LAST_NAME'] = $user['last_name'];
        $user_info['YOUR_EMAIL'] = $user['email'];
        $user_info['USER_EMAIL'] = $user['email'];

    endif;
    $user_info['SUPPORT_EMAIL'] = SUPPORT_EMAIL;
    $user_info['BCC_EMAIL'] = BCC_EMAIL;
    $user_info['SITE_NAME'] = SITE_NAME;
    $user_info['SALES_EMAIL'] = SALES_EMAIL;
    $user_info['FACEBOOK_PAGE'] = FACEBOOK_PAGE;
    $user_info['TWITTER_PAGE'] = TWITTER_PAGE;
    $user_info['YOUTUBE_PAGE'] = YOUTUBE_PAGE;
    return $user_info;
}

function bitly_shorten($url) {
    $query = array(
        "version" => "2.0.1",
        "longUrl" => $url,
        "login" => "o_7nn2mk5356", // replace with your login
        "apiKey" => "R_aaba4812c758bb7f77f1a48dca1d1b3d" // replace with your api key
    );

    $query = http_build_query($query);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://api.bit.ly/shorten?" . $query);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response);

    if ($response->errorCode == 0 && $response->statusCode == "OK") {
        return $response->results->{$url}->shortUrl;
    } else {
        return null;
    }
}

function getSettingsByName($name) {

    $QueryObj = new query('setting');

    $QueryObj->Where = "where name='" . $name . "'";

    $QueryObj->DisplayAll();

    $ObjArray = array();

    if ($QueryObj->GetNumRows()):

        while ($object = $QueryObj->GetArrayFromRecord()):

            $ObjArray[] = $object;

        endwhile;

    endif;

    return $ObjArray;
}

function show_age($date, $single = FALSE) {
    $age = strtotime($date);
    if ($age === false) {
        return false;
    }
    list($y1, $m1, $d1) = explode("-", date("Y-m-d", $age));
    $now = strtotime("now");
    list($y2, $m2, $d2) = explode("-", date("Y-m-d", $now));
    $age = $y2 - $y1;
    if ((int) ($m2 . $d2) < (int) ($m1 . $d1)) $age -= 1;
    if ($single === TRUE) return $age;
    return "<span class='age_calculate'>" . $age . '</span> Years';
}

function pr($e, $show_count = FALSE) {
    if ($show_count === TRUE && (is_array($e) || is_object($e))) {
        echo 'Count: ' . count($e) . '<hr />';
    }
    echo '<pre>';
    print_r($e);
    echo '</pre>';
}

function cats_loop($cats, $pre = '', $selected_id = 0, $main_cat = 0) {
//    pr($cats);
    ob_start();
    foreach ($cats as $cat) {
        if ($cat->id == $main_cat) continue;
        $obj = new foodCat;
        $sub_cats = $obj->getCategories($cat->id);
        $selected = '';
        if ($selected_id) {
            if (strpos($selected_id, ',')) {
                $selected_id = explode(',', $selected_id);
            }
            if (is_array($selected_id) && in_array($cat->id, $selected_id)) {
                $selected = " selected";
            } elseif ($cat->id == $selected_id) {
                $selected = " selected";
            }
        }
        ?>
        <option value="<?php echo $cat->id ?>"<?= $selected ?>><?php echo $pre . $cat->name ?></option>
        <?php
        if (!empty($sub_cats)) {
            $sub_pre = $pre . '|&nbsp;&nbsp;&nbsp;&nbsp;';
            echo cats_loop($sub_cats, $sub_pre, $selected_id, $main_cat);
        }
    }
    return ob_get_clean();
}
?>