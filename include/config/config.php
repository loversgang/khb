<?php

/* output buffering started */
ob_start();

/* set error level */
error_reporting(E_ALL && ~E_DEPRECATED);

/* start session by default */
session_start();

/*
 *   Code Developed By: cWebConsultants Team - India (Chandigarh)
 *   Project Name: <content managment system> - cWebConsultants
 *   Dated: <10 Jan, 2012>
 *   *** Copyrighted by cWebConsultants India - We reserve the right to take legal action against anyone using this software without our permission.  ***
 */

/* set document root */
define("DIR_FS", $_SERVER['DOCUMENT_ROOT'], true);

/* set website filesystem */
define("DIR_FS_SITE", dirname(dirname(dirname(__FILE__))) . '/', true);

/*
 * Be very carefully while setting these variables 
 * These are used in the URL Rewrite
 */
/*
 * set databse details here 
 */
$DBHostName = "localhost";
$DBDataBase = "khb";
$DBUserName = "root";
$DBPassword = "";

$is_local = false;
$is_bhukt = false;
$is_khb = false;
if (strpos($_SERVER['HTTP_HOST'], 'bhukt.com') !== false) {
    $is_bhukt = true;
} else {
    $is_local = true;
}
if ($is_local) {
    define("HTTP_SERVER", "http://localhost", true);
}
if ($is_bhukt) {
    define("DIR_WS_SITE", "http://bhukt.com/", true);
    $DBDataBase = "sumbria8_bhukt";
    $DBUserName = "sumbria8_dev";
    $DBPassword = "Winn3rs10090310$";
} else {
    define("DIR_WS_SITE", HTTP_SERVER . "/bhukt/", true);
}
if ($is_khb) {
    define("DIR_WS_SITE", "http://bhukt.com/", true);
    $DBDataBase = "cwebcons_khbsales";
    $DBUserName = "cwebcons_khbsale";
    $DBPassword = "R^4V4Zi4ogTm";
}

/*
 *   --- WARNING ---
 *  All the files below are location sensitive. 
 *  Maintain the sequence of files
 *
 */

# include sub-configuration files here.
require_once(DIR_FS_SITE . "include/config/url.php");

# include the database class files.
require_once(DIR_FS_SITE_INCLUDE_CLASS . "mysql.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS . "query.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS . "validation_u.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS . "validation_p.php");
# include session files here.
# include the utitlity files here
require_once(DIR_FS_SITE_INCLUDE_CLASS . "phpmailer.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "constant.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "message.php");

# custom files
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'login_session.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'admin_session.php');

# include functions here.
include_once(DIR_FS_SITE_INCLUDE_FUNCTION . 'date.php');
//require_once(DIR_FS_SITE_INCLUDE_FUNCTION."dBug.php");
# include function files here.
include_once(DIR_FS_SITE . 'include/function/basic.php');
include_once(DIR_FS_SITE . 'include/functionClass/errorClass.php');

#date_default_timezone_set('Asia/Calcutta');
# fix for stripslashes issues in php
if (get_magic_quotes_gpc()):
    $_POST = array_map_recursive('stripslashes', $_POST);
    $_GET = array_map_recursive('stripslashes', $_GET);
    $_REQUEST = array_map_recursive('stripslashes', $_REQUEST);
endif;
$publickey = "6LcWw7kSAAAAADZv_vEtAK2oEHvk3XcHuEoThrbN";
$privatekey = "6LcWw7kSAAAAACgQDdDBBIei-rxYTOPlgI02lDG9";
?>