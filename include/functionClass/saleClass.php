<?php

class sale extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('sales');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'sale_head_id', 'lot_id', 'buyer_id', 'amount', 'date_add');
    }

    function saveSale($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getSale($id) {
        return $this->_getObject('sales', $id);
    }

    function listSale($sale_head_id) {
        $obj = new query('sales,horses');
        $obj->Field = "sales.*,horses.name,horses.lot_number";
        $obj->Where = "where sales.lot_id=horses.id and sales.sale_head_id='$sale_head_id' and sales.sale_head_id=horses.sale_head_id";
        return $obj->ListOfAllRecords('object');
    }

    function listTrashSale() {
        $this->Where = "order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function deleteSale($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

    // Get All Sales
    function getTotalSales($sale_head_id) {
        $obj = new query('sales,horses,buyer');
        $obj->Field = "sales.id,sales.amount,sales.date_add,horses.name as horse_name,horses.lot_number,buyer.first_name,buyer.last_name";
        $obj->Where = "where sales.sale_head_id='$sale_head_id' and sales.buyer_id=buyer.id and sales.lot_id=horses.id and horses.sale_head_id=sales.sale_head_id";
        return $obj->ListOfAllRecords('object');
    }

}

class sale_head extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('sale_head');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'title', 'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    function saveSaleHead($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['upd_date'] = date('Y-m-d H:i:s');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['add_date'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getSaleHead($id) {
        return $this->_getObject('sale_head', $id);
    }

    function listAllSaleHeads() {
        return $this->ListOfAllRecords('object');
    }

    function listSaleHeads() {
        $this->Where = "where is_active='1'";
        return $this->ListOfAllRecords('object');
    }

    function listTrashSaleHead() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function deleteSaleHead($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

}
