<?php

class view extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('views');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'session_id', 'buyer_id', 'lot_id', 'view', 'spotter', 'vet', 'date_viewed');
    }

    function saveView($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_viewed'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getView($id) {
        return $this->_getObject('buyer', $id);
    }

    function listBuyers() {
        $this->Where = "where is_active='1' and is_deleted='0' order by first_name";
        return $this->ListOfAllRecords('object');
    }

    function listTrashBuyer() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function deleteBuyer($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

    public static function checkViewExists($session_id, $buyer_id, $lot_id) {
        $obj = new view;
        $obj->Where = "where session_id='$session_id' and buyer_id='$buyer_id' and lot_id='$lot_id'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : '';
    }

    // View Count
    public static function viewCount($horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where lot_id='$horse_id'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    // Buyer View Count
    public static function buyerViewCount($buyer_id, $horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where buyer_id='$buyer_id' and lot_id='$horse_id'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    function getBuyers($horse_id) {
        $this->Where = "where lot_id='$horse_id' group by buyer_id";
        return $this->ListOfAllRecords('object');
    }

    function getLotsTotalViewsReport($sale_head_id, $field = '') {
        $obj = new query("horses, views");
        $obj->Field = "views.*,horses.name,horses.lot_number,horses.sex";
        if ($field == 'spotter') {
            $obj->Where = "where horses.sale_head_id='$sale_head_id' and horses.id=views.lot_id and views.spotter='yes' group by views.lot_id";
        } elseif ($field == 'vet') {
            $obj->Where = "where horses.sale_head_id='$sale_head_id' and horses.id=views.lot_id and views.vet='yes' group by views.lot_id";
        } else {
            $obj->Where = "where (views.spotter='yes' or views.vet='yes' or views.view='yes') and horses.sale_head_id='$sale_head_id' and horses.id=views.lot_id group by views.lot_id";
        }
        #$obj->print=1;
        return $obj->ListOfAllRecords('object');
    }

    public static function horsetotalViewCount($horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where lot_id='$horse_id' and (spotter='yes' or vet='yes' or view='yes')";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function horsetotalViewCountByDate($horse_id, $date) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where lot_id='$horse_id' and CAST(date_viewed as DATE) = '$date' and (spotter='yes' or vet='yes' or view='yes')";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function horseBuyertotalViewCountByDate($horse_id, $date, $buyer_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where lot_id='$horse_id' and buyer_id='$buyer_id' and CAST(date_viewed as DATE) = '$date' and (spotter='yes' or vet='yes' or view='yes')";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function getLotViewsByDate($horse_id, $date) {
        $obj = new view;
        $obj->Where = "where lot_id='$horse_id' and CAST(date_viewed as DATE) = '$date' and (spotter='yes' or vet='yes' or view='yes')";
        return $obj->ListOfAllRecords('object');
    }

    public static function getBuyerLotViewsByDate($horse_id, $date, $buyer_id) {
        $obj = new view;
        $obj->Where = "where lot_id='$horse_id' and buyer_id='$buyer_id' and CAST(date_viewed as DATE) = '$date' and (spotter='yes' or vet='yes' or view='yes')";
        return $obj->ListOfAllRecords('object');
    }

    public static function horsetotalSpotterCount($horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where lot_id='$horse_id' and spotter='yes'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function horsetotalSpotterCountByBuyer($horse_id, $buyer_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where buyer_id='$buyer_id' and lot_id='$horse_id' and spotter='yes'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function horseTotalVetCount($horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where lot_id='$horse_id' and vet='yes'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function horseTotalVetCountByBuyer($horse_id, $buyer_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where buyer_id='$buyer_id' and lot_id='$horse_id' and vet='yes'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    function getHorseTotalReport($sale_head_id, $horse_id) {
        $obj = new query("horses,views");
        $obj->Field = "views.*,horses.name,horses.lot_number";
        $obj->Where = "where horses.sale_head_id='$sale_head_id' and horses.id='$horse_id' and horses.id=views.lot_id";
        return $obj->DisplayOne();
    }

    public static function getHorseBuyers($horse_id, $date) {
        $obj = new view;
        $obj->Field = "views.buyer_id";
        $obj->Where = "where (spotter='yes' or vet='yes' or view='yes') and lot_id='$horse_id' and CAST(date_viewed as DATE)='$date' group by buyer_id";
        $data = $obj->ListOfAllRecords('object');
        if (!$data) {
            return;
        }
        $ids = array();
        foreach ($data as $b_ids) {
            $ids[] = $b_ids->buyer_id;
        }
        $buyer_ids = implode(',', $ids);

        // Get Buyer Details
        $object = new query("buyer");
        $object->Where = "where id IN($buyer_ids)";
        return $object->ListOfAllRecords('object');
    }

    function getBuyerHorseReports($buyer_id, $sale_head_id) {
        $obj = new query("horses, views");
        $obj->Field = "views.*,horses.name,horses.lot_number,horses.sire,horses.dam";
        $obj->Where = "where buyer_id='$buyer_id' and horses.sale_head_id='$sale_head_id' and horses.id=views.lot_id group by views.lot_id";
        return $obj->ListOfAllRecords('object');
    }

    function getViewsByBuyer($buyer_id, $lot_id) {
        $this->Where = "Where buyer_id='$buyer_id' and lot_id='$lot_id'";
        return $this->ListOfAllRecords('object');
    }

    function getViewsByBuyerId($buyer_id) {
        $obj = new query("views,horses");
        $obj->Field = "views.*,horses.name,horses.lot_number,horses.sire,horses.dam";
        $obj->Where = "Where views.buyer_id='$buyer_id' and horses.id=views.lot_id group by views.lot_id";
        return $obj->ListOfAllRecords('object');
    }

    function getBuyersBuHorseId($lot_id) {
        $this->Field = "buyer_id";
        $this->Where = "where lot_id='$lot_id' group by buyer_id";
        $data = $this->ListOfAllRecords('object');
        $buyers_id_array = array();
        foreach ($data as $buyer) {
            $buyers_id_array[] = $buyer->buyer_id;
        }
        $buyer_ids = implode(',', $buyers_id_array);
        // Get Buyer Details
        $object = new query("buyer");
        $object->Where = "where id IN($buyer_ids)";
        return $object->ListOfAllRecords('object');
    }

    public static function horsetotalBuyerSpotterCount($buyer_id, $horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where buyer_id='$buyer_id' and lot_id='$horse_id' and spotter='yes'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function horsetotalBuyerVetCount($buyer_id, $horse_id) {
        $obj = new view;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where buyer_id='$buyer_id' and lot_id='$horse_id' and vet='yes'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    function get_views_date($buyer_id, $horse_id, $field) {
        $this->Where = "where buyer_id='$buyer_id' and lot_id='$horse_id' and `$field`='yes'";
        return $this->ListOfAllRecords('object');
    }

    function get_horse_total_views_date($horse_id) {
        $this->Where = "where lot_id='$horse_id' and (spotter='yes' or vet='yes' or view='yes')";
        return $this->ListOfAllRecords('object');
    }

    public static function getHorseViewDates() {
        $obj = new view;
        $obj->Field = "DISTINCT(CAST(date_viewed as DATE)) as date";
        $obj->Where = "where (spotter='yes' or vet='yes' or view='yes')";
        return $obj->ListOfAllRecords('object');
    }

    public static function getHorseViewDatesByHorseId($horse_id) {
        $obj = new view;
        $obj->Field = "DISTINCT(CAST(date_viewed as DATE)) as date";
        $obj->Where = "where lot_id='$horse_id' and (spotter='yes' or vet='yes' or view='yes')";
        return $obj->ListOfAllRecords('object');
    }

    public static function getHorseViewDatesByBuyerId($buyer_id) {
        $obj = new view;
        $obj->Field = "DISTINCT(CAST(date_viewed as DATE)) as date";
        $obj->Where = "where buyer_id='$buyer_id' and (spotter='yes' or vet='yes' or view='yes')";
        return $obj->ListOfAllRecords('object');
    }

    public static function getLotMinViewDate() {
        $obj = new view;
        $obj->Field = "MIN(date_viewed) as date";
        $obj->Where = "where (spotter='yes' or vet='yes' or view='yes')";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->date : '';
    }

}