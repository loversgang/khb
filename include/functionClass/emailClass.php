<?php

/*
 * Programs Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class email extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('email');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'email_name', 'email_subject', 'email_text', 'email_type');
    }

    function listEmails() {
        $this->Where = "";
        return $this->DisplayAll();
    }

    function saveEmail($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

}

?>