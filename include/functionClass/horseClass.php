<?php

class horse extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('horses');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'sale_head_id', 'name', 'lot_number', 'sire', 'dam', 'age', 'sex', 'status', 'is_active', 'is_deleted', 'date_add', 'date_upd');
    }

    function saveHorse($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['date_upd'] = date('Y-m-d H:i:s');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getHorse($id) {
        return $this->_getObject('horses', $id);
    }

    function listAllHorses() {
        $this->Where = "where is_deleted='0' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    function listHorses($sale_head_id) {
        $this->Where = "where is_deleted='0' and sale_head_id='$sale_head_id'";
        return $this->ListOfAllRecords('object');
    }

    function listTrashHorse() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function deleteHorse($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

}
