<?php

class buyer extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('buyer');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'first_name', 'last_name', 'email', 'phone', 'city', 'state', 'country', 'remarks', 'is_active', 'is_deleted', 'add_date', 'upd_date');
    }

    function saveBuyer($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['upd_date'] = date('Y-m-d H:i:s');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('Y-m-d H:i:s');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getBuyer($id) {
        return $this->_getObject('buyer', $id);
    }

    function listBuyers() {
        $this->Where = "where is_active='1' and is_deleted='0' order by first_name";
        return $this->ListOfAllRecords('object');
    }

    function listTrashBuyer() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function deleteBuyer($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

    function getBuyersDetail($ids) {
        $this->Where = "where id IN($ids) order by first_name";
        return $this->ListOfAllRecords('object');
    }

    public static function chkBuyerExists($first, $last) {
        $obj = new buyer;
        $obj->Where = "Where first_name='$first' and last_name='$last'";
        return $obj->DisplayOne();
    }

}
