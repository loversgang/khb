<?php

/*
 * Admin Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class admin extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position') {
        $this->InitilizeSQL();
        $this->orderby = $orderby;
        $this->order = $order;
        parent::__construct('admin');
        $this->TableName = 'admin_user';
        $this->requiredVars = array('id', 'user_type', 'username', 'password', 'first_name', 'last_name', 'email', 'last_access', 'last_sign_in', 'ip_address', 'is_deleted', 'is_active', 'is_loggedin', 'phone');
    }

    /*
     * Create new page or update existing page
     */

    function saveAdminUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($POST['password']) && $POST['password'] != '') {
            $this->Data['password'] = md5($POST['password']);
        }
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get page by id
     */

    function getAdminUser($id) {
        return $this->_getObject('admin_user', $id);
    }

    function check_admin_per($key, $com_array) {
        $test = '';
        if (array_key_exists($key, $com_array)):
            $test = "checked";
        endif;
        return $test;
    }

    /*
     * Get List of all pages in array
     */

    function listAdminUsers() {
        $this->Where = "where is_deleted='0' and user_type='user'";
        return $this->ListOfAllRecords('object');
    }

    /*
     * check admin user with name
     */

    function checkUsers($username, $email) {
        $this->Where = "where is_deleted='0' AND (username='$username' OR email='$email')";
        $this->DisplayAll();
    }

    // check field
    public static function checkFieldExists($field, $value, $id = 0) {
        $obj = new admin;
        if ($id == '0') {
            $obj->Where = "where $field='$value'";
        } else {
            $obj->Where = "where $field='$value' and id!='$id'";
        }
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : '';
    }

    /*
     * check admin user with name in update case
     */

    function checkUsersWithID($username, $email, $id) {
        $this->Where = "where is_deleted='0' AND id!='$id' AND (username='$username' OR email='$email')";
        $this->DisplayAll();
    }

    /*
     * delete a page by id
     */

    function deleteAdminUser($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

    // Get User Detail by Username

    public static function getUserByUsername($username) {
        $obj = new admin;
        $obj->Where = "where username='$username'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : '';
    }

}
