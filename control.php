<?php
require_once("include/config/config.php");
include DIR_FS_SITE . 'include/class/adminlog.php';
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/foodClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/otherSettingClass.php');

// Define Types
$user_type = $_SESSION['admin_session_secure']['user_type'] == 'user';
$admin_type = $_SESSION['admin_session_secure']['user_type'] == 'admin';

// Include Functions
$include_fucntions = array('video', 'date', 'email', 'file_handling', 'http',
    'image_manipulation', 'paging', 'news', 'testimonials',
    'recaptchalib', 'content', 'footer', 'gallery', 'users');

include_functions($include_fucntions);
if (!$admin_user->is_logged_in()):
    Redirect(DIR_WS_SITE . 'index.php');
endif;

$PageAccess = '*';

$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
?>
<div class="navbar">
    <div class="navbar-inner">
        <div class="left nav_left_logo">
            <a class="link" href="<?php echo make_load_url('home') ?>"><img src="assets/images/logo.png" alt="" title=""></a>
        </div>
        <div class="right" style="color: #fff;margin-right: 10px;">
            <font style="font-size: 15px">Logged In as</font><br/>
            <center>
                <b><?php echo ucfirst($_SESSION['admin_session_secure']['username']); ?></b>
            </center>
        </div>
    </div>
</div>
<div class="pages">
    <?php if ($Page == 'home') { ?>
        <div data-page="home" class="page" style="overflow: auto;background: #1D3D76;">
            <div class="loadingdata"></div>
        <?php } else { ?>
            <div data-page="<?php echo $Page . $_GET['action']; ?>" class="page">
                <div class="loadingdata"></div>
            <?php } ?>
            <?php
            if ($Page != '' && file_exists(DIR_FS_SITE . '/script/' . $Page . '.php')):
                if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):

                    if ($PageAccess != '*'):
                        $action = isset($_GET['action']) ? $_GET['action'] : "list";
                        if (isset($PageAccess[$Page][$action])):
                            include(DIR_FS_SITE . '/script/' . $Page . '.php');
                        endif;
                    else:
                        include(DIR_FS_SITE . '/script/' . $Page . '.php');
                    endif;
                endif;
            endif;
            if ($Page != "") {
                if (file_exists(DIR_FS_SITE . '/form/' . $Page . ".php")):
                    if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):
                        if ($PageAccess != '*'):
                            if (isset($PageAccess[$Page][$action])):
                                require_once(DIR_FS_SITE . '/form/' . $Page . ".php");
                            else:
                                echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                                echo "You do not have the permission to access this page.</p>";
                            endif;
                        else:
                            require_once(DIR_FS_SITE . '/form/' . $Page . ".php");
                        endif;
                    else:
                        echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                        echo "You do not have the permission to access this page.</p>";
                    endif;
                else:
                    if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):
                        if ($PageAccess != '*'):

                            if (isset($PageAccess[$Page][$action])):
                                require_once(DIR_FS_SITE . '/form/default.php');
                            else:
                                echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                                echo "You do not have the permission to access this page.</p>";
                            endif;
                        else:
                            require_once(DIR_FS_SITE . '/form/default.php');
                        endif;
                    else:
                        echo "<p style='height:250px;'><font size='3'><br><br><b>You do not have the permission to access this page.</b></font></p>";
                    endif;
                endif;
            }
            ?>
        </div>
    </div>
</div>