<?php if (isset($_GET['logged_out'])) { ?>
    <script>
        localStorage.khb_username = false;
        localStorage.khb_password = false;
        localStorage.removeItem('khb_username');
        localStorage.removeItem('khb_password');
    </script>
    <style>
        .loginform input.form_input{
            width: 100% !important;
        }
    </style>
    <?php
}
require_once("include/config/config.php");
require_once("include/functionClass/class.php");
require_once("include/functionClass/adminUserClass.php");

$function = array('url', 'cart', 'input', 'admin', 'users', 'gallery', 'database', 'url_rewrite');
include_functions($function);

/* check if already logged in */
if (isset($_COOKIE['khb_username'])) {
    $user_detail = admin::getUserByUsername($_COOKIE['khb_username']);
    if ($user_detail) {
        if ($user = validate_user('admin_user', (array) $user_detail, true)) {
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
            re_direct(DIR_WS_SITE_CONTROL . "admin.php");
        }
    }
}
if ($admin_user->is_logged_in()) {
    re_direct(DIR_WS_SITE_CONTROL . "admin.php");
}
require 'tmp/header.php';
?>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body id="mobile_wrap">
    <script>
        if (localStorage.khb_username && localStorage.khb_username !== false) {
            $('body').hide();
            //myApp.showIndicator();
            $.post('tmp/ajax.php', {act: 'user_login', username: localStorage.khb_username, password: localStorage.khb_password}, function (data) {
                var username = localStorage.khb_username;
                //myApp.hideIndicator();
                if (data === 'success') {
                    $.cookie('khb_username', username, {expires: 365, path: '/'});
                    window.location.href = "admin.php";
                } else if (data === 'error') {
                    toastr.error('Wrong Username or Password!', 'Error!');
                } else {
                    toastr.error('Something Went Wrong!', 'Error!');
                }
            });
        }
    </script>
    <div class="statusbar-overlay"></div>
    <div class="panel-overlay"></div>
    <div class="views">
        <div class="view view-main">
            <div class="pages  toolbar-through">
                <div data-page="index" class="page homepage">
                    <div class="page-content">
                        <div class="logo">
                            <img src="assets/images/logo.png" alt="" title="" />
                        </div> 
                        <div class="loginform" style="margin-top: 7%">
                            <form class="contactform" method="post" name="form_login">
                                <input autofocus type="text" id="username" name="username" value="" class="form_input homepage-input" placeholder="Username" />
                                <input type="password" id="password" name="password" value="" class="form_input" placeholder="Password" />
                                <input type="submit" id="user_form_submit" name="login" class="button button-big button-fill" value="SIGN IN" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Bottom Toolbar-->
            <div class="toolbar">
                <div class="toolbar-inner">
                    <ul class="toolbar_icons">
                        <li class=""><a href="#" ><?php echo date('Y') ?> @ <?= SITE_NAME ?></a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </div>
    <!-- Login Popup -->
    <div class="popup popup-forgot">
        <div class="content-block-login">
            <h4>FORGOT PASSWORD</h4>
            <div class="form_logo"><img src="assets/images/logo.png" alt="" title="" /></div>
            <div class="loginform">
                <form id="LoginForm" method="post">
                    <input type="text" name="Email" value="" class="form_input required" placeholder="email" />
                    <input type="submit" name="submit" class="form_submit" id="submit" value="RESEND PASSWORD" />
                </form>
                <div class="forgot_pass homepage-forgot_pass"><a href="#" data-popup=".popup-forgot" class="open-popup">Forgot Password?</a></div>
                <div class="signup_bottom">
                    <p>Check your email and follow the instructions to reset your password.</p>
                </div>
            </div>
            <div class="close_loginpopup_button"><a href="#" class="close-popup"><img src="assets/images/icons/white/menu_close.png" alt="" title="" /></a></div>
        </div>
    </div>
    <?php require 'tmp/footer.php'; ?>
</body>
<!-- END BODY -->
</html>